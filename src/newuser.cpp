#include "newuser.h"
#include "QMessagebox.h"
#include "ui_newuser.h"

/*!
 * \fn NewUser::NewUser
 * \brief Constructeur de l'interface d'ajout d'un utilisateur.
 * \param db La base de données.
 * \param parent
 */
NewUser::NewUser(UserDao &db,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewUser),
    userDbAccess(db)
{
    ui->setupUi(this);
    connect(ui->ok,SIGNAL(pressed()),this,SLOT(CloseAndSave()));
    connect(ui->annuler,SIGNAL(pressed()),this,SLOT(close()));
}

/*!
 * \fn NewUser::Save
 * \brief Ajoute un nouvel utilisateur à la base de données.
 * \return True si le processus d'ajout se déroule correctement, sinon False dans le cas contraire.
 */
bool NewUser::Save()
{
    User user;
    user.name = ui->nom->text();
    user.isAdmin = ui->admin->isChecked();
    if(ui->mdp1->text() == ui->mdp2->text() && ui->mdp1->text().size() >= 3 && user.name.size() == 3)
    {
        userDbAccess.addUser(user,ui->mdp1->text());
        return true;
    }
    return false;
}

/*!
 * \fn NewUser::CloseAndSave
 * \brief Permet de fermet l'interface et d'ajouter l'utilisateur. Informe l'utilisateur si la sauvegarde du nouvel utilisateur a échoué (paramètres incorrects).
 */
void NewUser::CloseAndSave()
{
    if(Save())
    {
        emit UpdatingUserNeeded();
        this->close();
    }
    else
    {
        QMessageBox(QMessageBox::Warning, "Sauvegarde de l'utilisateur échoué", "Certains paramètres sont incorrects", QMessageBox::Ok,this).exec();
    }
}

/*!
 * \fn NewUser::~NewUser
 * \brief Destructeur de l'interface d'ajout d'un utilisateur.
 */
NewUser::~NewUser()
{
    delete ui;
}
