#ifndef FICHERECETTEDAOSQL_H
#define FICHERECETTEDAOSQL_H
#include "FicheRecetteDao.h"
#include "MainWindow.h"

class FicheRecetteDaoSql: public FicheRecetteDao
{
public:
    explicit FicheRecetteDaoSql(AsyncDatabaseAccess& db);
    virtual QFuture<FicheRecetteResult> getFicheRecette(const int &id);
    virtual QFuture<FicheRecetteResult> getLastFicheRecette(const QString &pn);
    virtual QFuture<Error> addFicheRecette(const FicheRecette &fiche);
    virtual QFuture<Error> removeFicheRecette(const int &id) ;
    virtual QString getLastError() ;
    virtual QFuture<QList<FicheRecetteResult>> getListFichesRecettes();
    virtual QFuture<QString> getJsonRecette(const QString &name);
    virtual QFuture<QString> getJsonRecette(const int &id);
    virtual QFuture<Error> alterFicheRecette(const FicheRecette &fiche);
};

#endif // FICHERECETTEDAOSQL_H


