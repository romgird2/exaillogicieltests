#include "informationtests.h"
#include "ui_informationtests.h"
#include <random>
#include <QBarSeries>
#include <QBarSet>
#include <QValueAxis>
#include <QBarCategoryAxis>

/*!
 * \fn InformationTests::InformationTests
 * \brief Création de l'interface d'affichage des résultats de tests.
 * \param db La base de données.
 * \param parent
 */
InformationTests::InformationTests(AsyncDatabaseAccess & db, QWidget *parent) :
    QDialog(parent, Qt::Window),
    ui(new Ui::InformationTests)
{
    ui->setupUi(this);
    //Style::putStyle(this);
    //ui->pushButton->setStyleSheet("QPushButton{font-size: 15px;}");
    testDbAccess = std::make_unique<TestDaoSql>(db);
    ficheDbAccess = std::make_unique<FicheRecetteDaoSql>(db);
    resultsDbAccess = std::make_unique<ResultsStepsDaoSql>(db);
    connect(this, &InformationTests::listFicheWidgetObtained, this, &InformationTests::setUpListWidgetFiche);
    connect(ui->listWidgetFiches, &QListWidget::itemActivated, this, &InformationTests::fetchSteps);
    connect(this, &InformationTests::jsonObtained, this, &InformationTests::setupSteps);
    connect(ui->listWidgetSteps, &QListWidget::itemDoubleClicked, this, &InformationTests::showStepMessageBox);
    connect(ui->listWidgetSteps, &QListWidget::itemClicked, this, [this](QListWidgetItem* item){
        requestGraphData(currentFicheId,ui->listWidgetSteps->row(item));
    });
    connect(this, &InformationTests::valuesObtained, this, &InformationTests::setUpGraph);
    connect(this, &InformationTests::allStepsObtained, this, &InformationTests::setUpFicheGraph);
    ficheDbAccess->getListFichesRecettes().then([this](QList<FicheRecetteResult> result){
        QList<FicheRecette> fiches;
        for(auto& fiche : result){
            fiches.append(fiche.fiche);
        }
        emit listFicheWidgetObtained(fiches);
    });
    this->showMaximized();
    ui->horizontalWidgetCounts->hide();
    ui->pushButtonGlobalView->hide();
    connect(ui->pushButtonRapport,&QPushButton::clicked,this,&InformationTests::savePdf);
    //connect(ui->pushButtonGlobalView,&QPushButton::clicked,this,&InformationTests::setUpFicheGraph);
}

/*!
 * \fn InformationTests::savePdf
 * \brief Enregistre au format PDF les résultats des tests.
 */
void InformationTests::savePdf()
{
    Style::GeneratePDF(this->allStepsForCurrentFiche,this);
}

/*!
 * \fn InformationTests::~InformationTests
 * \brief Destructeur de l'interface d'affichage des résultats de tests.
 */
InformationTests::~InformationTests()
{
    delete ui;
}

/*!
 * \fn InformationTests::setUpListWidgetFiche
 * \brief Met en place le widget représentant la fiche recette indiquée.
 * \param fiches
 */
void InformationTests::setUpListWidgetFiche(QList<FicheRecette> fiches)
{
    ui->listWidgetFiches->clear();
    for(auto& fiche : fiches){
        QListWidgetItem* item = new QListWidgetItem(QString("%1 - %2").arg(fiche.Date.toString("dd/MM/yyyy"),fiche.name), ui->listWidgetFiches);
        item->setData(Qt::UserRole,fiche.id);
    }
}

/*!
 * \fn InformationTests::setUpListWidgetSteps
 * \brief Met en place le widget représentant l'étape renseignée de la fiche recette.
 * \param steps L'étape à renseigner.
 */
void InformationTests::setUpListWidgetSteps(QList<QString> steps)
{
    for(auto& step : steps){
        ui->listWidgetFiches->addItem(step);
    }
}

/*!
 * \fn InformationTests::fetchSteps
 * \brief Récupère les étapes sous format JSON.
 * \param item La fiche recette dont on souhaite obtenir les étapes au format JSON.
 */
void InformationTests::fetchSteps(QListWidgetItem* item)
{
    currentFicheId = item->data(Qt::UserRole).toInt();
    ficheDbAccess->getJsonRecette(currentFicheId).then([this](QString json){
        emit jsonObtained(json);
    });
}

/*!
 * \fn InformationTests::setupSteps
 * \brief Met en place les widgets des différentes étapes avec les étapes renseignées dans le JSON.
 * \param json Le JSON renseignant les étapes.
 */
void InformationTests::setupSteps(QString json)
{
    ui->listWidgetSteps->clear();
    recette = std::make_unique<Recette>(json);
    int counter = 0;
    for(auto& step : recette->steps){
        ui->listWidgetSteps->addItem(QString("%1 - %2").arg(++counter).arg(step.typeToString()));
    }
    resultsDbAccess->getAllResultM(currentFicheId).then([this](std::shared_ptr<ResultMResult> res){
        if(res->error == ResultsStepsDaoSql::NO_ERROR){
            emit allStepsObtained(res);
        }
    });
}

/*!
 * \fn InformationTests::showStepMessageBox
 * \brief Affiche un message décrivant l'étape de la fiche recette.
 * \param item L'étape de la fiche recette dont on souhaite afficher la description.
 */
void InformationTests::showStepMessageBox(QListWidgetItem* item)
{
    if(recette){
        recette->steps[ui->listWidgetSteps->row(item)].toMessageBox();
    }
}

/*!
 * \fn InformationTests::requestGraphData
 * \brief Demande les résultats relatifs à une étape d'une fiche recette.
 * \param id_fiche L'ID de la fiche recette.
 * \param pos_in_fiche La position/l'index de l'étape dont on souhaite les résultats.
 */
void InformationTests::requestGraphData(int id_fiche, int pos_in_fiche)
{
    resultsDbAccess->getResultM(id_fiche,pos_in_fiche).then([this](ResultMResult res){
        if(res.error == ResultsStepsDao::NO_ERROR){
            QList<double> values;
            for(auto& r : res.res_m){
                values.append(r.mesure);
            }
            currentStepUnitName = recette->steps[res.res_m[0].pos_in_fiche].unit_name;
            emit valuesObtained(values, res.res_m[0].min_value, res.res_m[0].max_value);
        }
    });
}

/*!
 * \fn InformationTests::setUpGraph
 * \brief Met en place le graphique représentant les valeurs renseignées qui doivent être comprises entre une valeur minimale et maximale.
 * \param values Les valeurs que l'on veut afficher.
 * \param min_value La valeur minimale.
 * \param max_value La valeur maximale.
 */
void InformationTests::setUpGraph(QList<double> values, double min_value, double max_value)
{
    std::sort(values.begin(),values.end());
    QList<double> bins;
    constexpr int binsCountInRange = 6;
    double binsWidth = (max_value - min_value)/binsCountInRange;
    double minimum = values[0];
    double maximum = values.back();
    double currentBinValue = minimum;
    while(currentBinValue < min_value){
        bins.append(currentBinValue);
        currentBinValue+=binsWidth;
    }
    for(int i=0;i<binsCountInRange;i++){
        double last = bins.size() == 0 ? min_value : bins.last();
        bins.append(last+binsWidth);
    }
    currentBinValue = maximum;
    while(currentBinValue < maximum){
        bins.append(currentBinValue);
        currentBinValue+=binsWidth;
    }
    QList<int> counts(bins.size(),0);
    double mean = 0.0;
    int nok_count = 0;
    int ok_count = 0;
    for(double value : values){
        int binPos = 0;
        while(binPos < bins.size()-2 && bins[++binPos] < value){}
        counts[binPos-1] += 1;
        if(min_value > value || value > max_value){
            nok_count++;
        }
        else {
            ok_count++;
        }
        mean += value;
    }
    mean /= values.size();
    ui->horizontalWidgetCounts->show();
    ui->labelNValues->setText(QString("Valeurs : %1").arg(values.size()));
    ui->labelMin->setText(QString("Min : %1").arg(minimum));
    ui->labelMax->setText(QString("Max : %1").arg(maximum));
    ui->labelMean->setText(QString("Mean : %1").arg(mean));
    ui->labelOK->setText(QString("OK : %1").arg(ok_count));
    ui->labelNOK->setText(QString("NOK : %1").arg(nok_count));
    QBarSeries* series = new QBarSeries();
    QBarSet* set = new QBarSet("Values");
    for(int i = 0; i < bins.size(); i++){
        *set << counts[i];
    }
    series->append(set);
    QValueAxis* axis = new QValueAxis();
    axis->setRange(bins[0]-binsWidth,bins.last());
    axis->setTickCount(counts.size());
    axis->setTitleText(QString("Mesure (%1)").arg(currentStepUnitName));
    QValueAxis* axisY = new QValueAxis();
    axisY->setRange(0,*std::max_element(counts.begin(),counts.end()));
    axisY->applyNiceNumbers();
    axisY->setTitleText(QString("Nombre de mesures"));
    ui->chartsView->setChart(new QChart());
    ui->chartsView->chart()->addSeries(series);
    ui->chartsView->chart()->addAxis(axis, Qt::AlignBottom);
    ui->chartsView->chart()->addAxis(axisY, Qt::AlignLeft);
}

/*!
 * \fn InformationTests::setUpFicheGraph
 * \brief Met en place le graphique récapitulant la réussite ou non des différentes étapes d'une fiche recette.
 * \param steps Les étapes de la fiche recette.
 */
void InformationTests::setUpFicheGraph(std::shared_ptr<ResultMResult> steps)
{
    ui->pushButtonGlobalView->setEnabled(true);
    ui->pushButtonRapport->setEnabled(true);
    allStepsForCurrentFiche = steps;
    QMap<int,int> stepsFailuresCount;
    for(auto& step : steps->res_m){
        if(step.min_value > step.mesure || step.mesure > step.max_value){
            stepsFailuresCount[step.pos_in_fiche] += 1;
        }
    }
    QList<QPair<int,int>> stepsList;
    stepsList.reserve(stepsFailuresCount.size());
    for(auto& key : stepsFailuresCount.keys()){
        stepsList.append({key,stepsFailuresCount[key]});
    }
    std::sort(stepsList.begin(),stepsList.end(),[](auto& v1, auto& v2){
        return v1.second < v2.second;
    });
    QBarSeries* series = new QBarSeries();
    QBarSet* set = new QBarSet("NOK par étape");
    QStringList categories;
    int maxNok = 0;
    for(auto& v : stepsList.mid(0,stepsList.size() < 10 ? stepsList.size() : 10)){
        *set << v.second;
        categories << QString::number(v.first+1);
        if(v.second > maxNok){
            maxNok = v.second;
        }
    }
    series->append(set);
    QBarCategoryAxis* axis = new QBarCategoryAxis();
    axis->setCategories(categories);
    axis->setTitleText("Numéro de l'étape");
    QValueAxis* axisY = new QValueAxis();
    axisY->setRange(0,maxNok);
    axisY->applyNiceNumbers();
    axisY->setTitleText("Nombre de NOK");
    ui->chartsView->setChart(new QChart());
    ui->chartsView->chart()->addSeries(series);
    ui->chartsView->chart()->addAxis(axis, Qt::AlignBottom);
    ui->chartsView->chart()->addAxis(axisY, Qt::AlignLeft);
}
