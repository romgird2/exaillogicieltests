#ifndef RECETTE_H
#define RECETTE_H

#include <QString>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include "qexception.h"
#include "qjsonarray.h"
#include "stepRecette.h"
class Recette
{
public:

    static const QString unit_factor_name[];
    static const double unit_factor[];
    static const QString unit_name[];
    static const int nb_unit_factor;
    static const int nb_unit;

    QString toJson();
    void AddStep(const stepRecette &step);
    void RemoveStep(const int &position);
    stepRecette GetStep(const int &position);
    stepRecette::type_recette match_type(const QString &type);
    double match_unit(const QString &unit);

    QString titre;
    QString PN;
    QList<stepRecette> steps;
    Recette(QString json);

};

#endif // RECETTE_H
