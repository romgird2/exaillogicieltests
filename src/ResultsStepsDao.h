#ifndef RESULTSSTEPSDAO_H
#define RESULTSSTEPSDAO_H

#include <QObject>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include "AsyncDatabaseAccess.h"

struct ResultM {
    int id_fiche;
    int pos_in_fiche;
    double min_value;
    double max_value;
    double mesure;
};

struct ResultS {
    int id_fiche;
    int pos_in_fiche;
    bool script_status;
    QString script_log;
};

struct ResultMResult;

struct ResultSResult;

class ResultsStepsDao
{
public:
    explicit ResultsStepsDao(AsyncDatabaseAccess& db);
    virtual ~ResultsStepsDao();
    enum Error {
        INVALID_ID_FICHE,
        INVALID_POS_IN_FICHE,
        INVALID_MESURE,
        INVALID_SCRIPT_STATUT,
        INVALID_SCRIPT_LOG,
        RESULT_MESURE_ALREADY_EXISTS,
        RESULT_SCRIPT_ALREADY_EXISTS,
        SQL_ERROR,
        NO_ERROR
    };
    virtual QFuture<ResultMResult> getResultM(const int &id_fiche, const int &pos_in_fiche) = 0;
    virtual QFuture<ResultSResult> getResultS(const int &id_fiche, const int &pos_in_fiche) = 0;
    virtual QFuture<std::shared_ptr<ResultMResult>> getAllResultM(const int &id_fiche) = 0;
    virtual QFuture<Error> addResultM(const ResultM &res_m) = 0;
    virtual QFuture<Error> addResultS(const ResultS &res_s) = 0;
    virtual QFuture<Error> removeResultM(const int &id_fiche, const int &pos_in_fiche) = 0;
    virtual QFuture<Error> removeResultS(const int &id_fiche, const int &pos_in_fiche) = 0;
    virtual QFuture<Error> modifyResultM(const ResultM& res_m) = 0;
    virtual QFuture<Error> modifyResultS(const ResultS& res_s) = 0;
    virtual QString getLastError() = 0;


protected:
    QString lastError;
    AsyncDatabaseAccess& db;

};

struct ResultMResult {
    QList<ResultM> res_m;
    ResultsStepsDao::Error error;
};

struct ResultSResult {
    ResultS res_s;
    ResultsStepsDao::Error error;
};

#endif // RESULTSSTEPSDAO_H
