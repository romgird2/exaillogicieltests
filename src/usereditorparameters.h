#ifndef USEREDITORPARAMETERS_H
#define USEREDITORPARAMETERS_H

#include "UserDao.h"
#include "qdialog.h"
#include <QWidget>

namespace Ui {
class UserEditorParameters;
}

class UserEditorParameters : public QDialog
{
    Q_OBJECT

public:
    explicit UserEditorParameters(UserDao &db,QString trigramme,User user,QWidget *parent = nullptr);
    ~UserEditorParameters();

    void closeEvent(QCloseEvent *event);

public slots:
    void Save();

    void ResetMdp();

    void Delete();

signals:

    void UpdatingUserNeeded();

private:
    QString name;
    bool needUpdate;
    Ui::UserEditorParameters *ui;
    QString userTrigramme;
    UserDao& userDbAccess;
};

#endif // USEREDITORPARAMETERS_H
