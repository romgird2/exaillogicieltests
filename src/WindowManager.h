#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

#include "WindowManagerTypes.h"
#include "connexiondialog.h"
#include "MainWindow.h"
#include "jsoneditor.h"
#include "mainproduction.h"
#include "parameters.h"
#include "AsyncDatabaseAccess.h"
#include "informationtests.h"

/*!
 * Cette classe est un gestionnaire de fenêtres
 * Chaque fenêtre hérite de la classe RegisteredWindow
 * Chaque fenêtre renvoie un std::variant contenant une structure qui décrit la fenêtre suivante à ouvrir après sa fermeture
 * ainsi que les paramètres à envoyer à la fenêtre qui va être ouverte
 * Cette classe surcharge l'opérateur () pour créer la bonne fenêtre depuis la structure renvoyées
 * Cet opérateur est appelé par std::visit qui sélectionne la bonne méthode en fonction du type stocké dans le std::variant
 * std::visit se comporte comme un pattern matching, les opérateurs () correspondent au différents cas du matching.
 */
struct WindowManager {
    AsyncDatabaseAccess& db;
    WindowManager(AsyncDatabaseAccess & db) : db(db){
    }
    template<typename T, typename... Us>
    WindowManagerReturn runWindow(Us&... args){
        auto window = std::make_unique<T>(args...);
        window->exec();
        return window->managerReturn();
    }
    WindowManagerReturn operator()(ConnexionDialogV & param){
        return runWindow<ConnexionDialog>(db);
    }
    WindowManagerReturn operator()(MainWindowV & param){
        if(param.dialogStatus == QDialog::Accepted)
            return runWindow<MainWindow>(db, param);
        else
            return ExitV();
    }
    WindowManagerReturn operator()(ParametersV & param){
        return runWindow<Parameters>(db);
    }
    WindowManagerReturn operator()(JsonEditorV & param){
        return runWindow<jsonEditor>(db, param.trigramme);
    }
    WindowManagerReturn operator()(MainProductionV & param){
        return runWindow<MainProduction>(db,param);
    }
    WindowManagerReturn operator()(InformationTestsV & param){
        return runWindow<InformationTests>(db);
    }
    WindowManagerReturn operator()(ExitV & param){
        // This case should not happen if main is implemented correctly, testing for holding of ExitV in variant
        // and returning proprely.
        std::exit(0);
    }
};

#endif // WINDOWMANAGER_H
