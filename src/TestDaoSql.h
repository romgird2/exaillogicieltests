#ifndef TESTDAOSQL_H
#define TESTDAOSQL_H

#include "TestDao.h"

class TestDaoSql : public TestDao
{
public:
    TestDaoSql(AsyncDatabaseAccess& db);
    virtual QFuture<TestResult> getTest(const int &id) override;
    virtual QFuture<Error> addTest(const Test &test) override;
    virtual QFuture<Error> removeTest(const int &id) override;
    virtual QFuture<bool> checkUser(const QString &trigramme, const int &ficheId) override;
private:
    const static int N_TEST_COMPETENCE = 10;
};

#endif // TESTDAOSQL_H
