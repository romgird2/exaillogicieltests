#include "mainproduction.h"
#include "ui_mainproduction.h"
#include "style.h"

/*!
 * \fn MainProduction::MainProduction
 * \brief Constructeur de l'interface de production.
 * \param db La base de données.
 * \param params Les paramètres liés à l'instanciation de l'interface de production.
 * \param parent
 */
MainProduction::MainProduction(AsyncDatabaseAccess &db, MainProductionV & params, QWidget *parent) :
    QDialog(parent, Qt::Window),
    ui(new Ui::MainProduction)
{
    ui->setupUi(this);
    Style::putStyle(this);
    ui->progressBar->setValue(0);
    ui->labelDesc->hide();
    ui->labelPoints->hide();
    ui->lineEditMesure->hide();
    ui->pushButtonOkStep->hide();
    ui->pushButtonOkStep->setDefault(false);
    ui->progressBarScript->setVisible(false);
    ui->labelTitreOf->setText(params.of.id_text);
    ui->listWidget_2->hide();
    ui->labelCartesRestantes->setText(QString("%1/%2").arg(params.of.nb_cartes-params.of.nb_restant).arg(params.of.nb_cartes));
    currentScript = new QProcess(this);
    isAdmin = params.isAdmin;
    trigramme = params.trigramme;
    currentOf = params.of;
    pnText = params.pn;
    snText = params.sn;
    id_fiche = params.fiche_id;
    connect(this, &MainProduction::ficheRecieved, this, &MainProduction::setupRecette);
    connect(ui->listWidget, &QListWidget::itemDoubleClicked, this, [this](QListWidgetItem* item){
        if(recette){
            int row = ui->listWidget->row(item);
            recette->steps.at(row).toMessageBox(row);
        }
    });
    connect(ui->pushButtonOkStep, &QPushButton::clicked, this, &MainProduction::setNextStep);
    connect(currentScript, &QProcess::readyReadStandardOutput, this, &MainProduction::handleScriptOutput);
    connect(this, &MainProduction::returnToMainWindow, this, [this](){
        MainWindowV result;
        result.dialogStatus = QDialog::Accepted;
        result.isAdmin = isAdmin;
        result.trigramme = trigramme;
        result.of_field = currentOf.id_text;
        result.pn_field = pnText;
        returnCode = result;
        QDialog::accept();
    });
    ficheDbAccess = std::make_unique<FicheRecetteDaoSql>(db);
    ofDbAccess = std::make_unique<OrdreFabricationDaoSql>(db);
    testDbAccess = std::make_unique<TestDaoSql>(db);
    ficheDbAccess->getJsonRecette(params.fiche_id).then([this](QString json){
        emit ficheRecieved(json);
    });
}

/*!
 * \fn MainProduction::~MainProduction
 * \brief Destructeur de l'interface de production.
 */
MainProduction::~MainProduction()
{
    delete ui;
}

/*!
 * \brief MainProduction::setupRecette
 * \brief Charge une fiche recette à partir de son JSON.
 * \param json JSON de la fiche recette.
 */
void MainProduction::setupRecette(QString json)
{
    if(json == ""){
        QMessageBox::critical(this,"Erreur de fiche recette", "Il y a une erreur sur la fiche recette. Veuillez contacter un administrateur.");
    }
    else {
        recette = std::make_unique<Recette>(json);
        this->setWindowTitle(recette->titre);
        ui->progressBar->setMaximum(recette->steps.size());
        fillListWidget();
        setUpCenterWidget();
    }
}

/*!
 * \fn MainProduction::fillListWidget
 * \brief Remplis la liste de l'interface de production avec les étapes.
 */
void MainProduction::fillListWidget()
{
    if(recette){
        int counter = 1;
        for(auto& step : recette->steps){
            ui->listWidget->addItem(QString("%1 - %2").arg(counter++).arg(step.typeToString()));
        }
    }
    ui->listWidget->setMinimumWidth(ui->listWidget->sizeHintForColumn(0) + ui->listWidget->frameWidth() * 2 + this->style()->pixelMetric(QStyle::PM_ScrollBarExtent) + 10);
}

/*!
 * \fn MainProduction::setUpCenterWidget
 * \brief Met en place le module central de l'interface de production.
 */
void MainProduction::setUpCenterWidget()
{
    if(currentStepIndex >= recette->steps.size()){
        ui->pushButtonOkStep->setText("Quitter [Entrée]");
            ui->labelDesc->setText("Fin du test");
        ui->labelPoints->setText(QString("Test %1 : %2 OK - %3 NOK").arg(failedSteps.size() > 0 ? "NOK" : "OK").arg(succededSteps.size()).arg(failedSteps.size()));
        return;
    }
    stepRecette & currentStep = recette->steps[currentStepIndex];
    if(currentStep.type == stepRecette::texte){
        ui->labelDesc->setVisible(true);
        ui->labelDesc->setText(currentStep.description);
        if(currentStepIndex < recette->steps.size()-1 && recette->steps[currentStepIndex+1].type == stepRecette::mesure){
            currentStep = recette->steps[currentStepIndex+1];
            currentStepIndex++;
            QString pointsText = "";
            if (currentStep.references.size() >= 1) {
              pointsText = "Points : ";
              for (auto &point :
                   currentStep.references) {
                pointsText += (point + " ");
              }
              pointsText += " - ";
            }
            ui->labelPoints->setVisible(true);
            ui->labelPoints->setText(pointsText + "Unité : " + currentStep.unit_name);
            ui->lineEditMesure->setVisible(true);
            ui->lineEditMesure->clear();
            ui->lineEditMesure->setFocus();
            nextIndexIncrease = 1;
        }
        else {
            nextIndexIncrease = 1;
        }
        ui->pushButtonOkStep->setVisible(true);
        ui->pushButtonOkStep->setDefault(true);
        ui->progressBarScript->setVisible(false);
    }
    else if(currentStep.type == stepRecette::script) {
        ui->lineEditMesure->hide();
        ui->labelDesc->setText("Script :");
        ui->labelPoints->setText(currentStep.name);
        ui->progressBarScript->setVisible(true);
        ui->progressBarScript->setValue(0);
        scriptResult = false;
        if(QFile::exists(currentStep.name)){
            currentScript->start("python", QStringList() << currentStep.name);
        }
        else {
            QMessageBox::warning(this, "Erreur de script",QString("Le script %1 n'existe pas.").arg(currentStep.name));
        }
        ui->pushButtonOkStep->setEnabled(false);
    }
    ui->groupBoxCurrentStep->setTitle(QString("Etape actuelle : %1 / %2").arg(currentStepIndex+1).arg(recette->steps.size()));
    ui->listWidget->item(currentStepIndex)->setBackground(this->palette().color(QPalette::WindowText));
    ui->listWidget->item(currentStepIndex)->setForeground(this->palette().color(QPalette::Window));
    ui->listWidget->setCurrentRow(currentStepIndex);
}

/*!
 * \fn MainProduction::setNextStep
 * \brief Met en place la prochaine étape sur l'interface de production.
 */
void MainProduction::setNextStep(){
    if(currentStepIndex >= recette->steps.size()){
        currentOf.nb_restant -= 1;
        ofDbAccess->modifyOF(currentOf).then([this](OrdreFabricationDao::Error res){
            Test testResult;
            testResult.sn = snText;
            testResult.date = QDate::currentDate();
            testResult.data = failedSteps.size() > 0 ? "NOK" : "OK";
            testResult.fiche_recette = id_fiche;
            testResult.user = trigramme;
            testDbAccess->addTest(testResult).waitForFinished();
            emit returnToMainWindow();
        });
        return;
    }
    stepRecette & currentStep = recette->steps[currentStepIndex];
    auto textColor = this->palette().color(QPalette::WindowText);
    constexpr auto failedColor = Qt::darkRed;
    constexpr auto successColor = Qt::darkGreen;
    if(currentStep.type == stepRecette::mesure){
        double value = ui->lineEditMesure->text().toDouble();
        if(currentStep.valeurs.size() == 1){
            if(ui->lineEditMesure->text().toDouble() == currentStep.valeurs[0]){
                textColor = successColor;
            }
            else {
                textColor = failedColor;
            }
        }
        else if(currentStep.valeurs.size() == 2){
            if(currentStep.valeurs[0] <= value && value <= currentStep.valeurs[1]){
                textColor = successColor;
            }
            else {
                textColor = failedColor;
            }
        }
    }
    else if(currentStep.type == stepRecette::script){
        textColor = scriptResult ? successColor : failedColor;
    }
    else {
        textColor = successColor;
    }
    if(textColor == failedColor){
        failedSteps.append(currentStep);
    }
    else if(textColor == successColor){
        succededSteps.append(currentStep);
    }
    ui->listWidget->item(currentStepIndex)->setBackground(this->palette().color(QPalette::Window));
    ui->listWidget->item(currentStepIndex)->setForeground(textColor);
    currentStepIndex += nextIndexIncrease;
    ui->progressBar->setValue(currentStepIndex);
    setUpCenterWidget();
}

/*!
 * \fn MainProduction::handleScriptOutput
 * \brief Permet de prendre en compte les sorties consoles des scripts.
 */
void MainProduction::handleScriptOutput()
{
    QString textData = currentScript->readAllStandardOutput();
   
    if(textData.size() >= 11 && textData.first(10) == "[PROGRESS]"){
        bool conversionOk = false;
        int progressionValue = textData.mid(10).toInt(&conversionOk);
        if(conversionOk && progressionValue <= 100 && progressionValue >= 0){
            ui->progressBarScript->setValue(progressionValue);
        }
    }
    else if(textData.size() >= 10 && textData.first(8) == "[RESULT]"){
        scriptResult = !(textData[8] == 'N');
        ui->pushButtonOkStep->setEnabled(true);
    }
}
