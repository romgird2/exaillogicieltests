#ifndef ASYNCDATABASEACCESS_H
#define ASYNCDATABASEACCESS_H

#include <QObject>
#include <QThreadPool>
#include <QSqlDatabase>
#include <QtConcurrent/QtConcurrent>
#include <QSqlQuery>
#include <functional>

class AsyncDatabaseAccess : QObject
{
    Q_OBJECT
public:
    AsyncDatabaseAccess(QObject * parent);
    ~AsyncDatabaseAccess();
    QFuture<bool> open(const QString& host, const int& port, const QString& name, const QString& user, const QString& password);
    QThreadPool* getThreadPoolPtr();

private:
    QThreadPool threadPool;
    QSqlDatabase *database;
    const QString driver = "QPSQL";
};

#endif // ASYNCDATABASEACCESS_H
