#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <QFuture>
#include <QMessageBox>
#include <QInputDialog>
#include "AsyncDatabaseAccess.h"
#include "FicheRecetteDao.h"
#include "RegisteredWindow.h"
#include "TestDaoSql.h"
#include "CarteElecDaoSql.h"
#include "UserDaoSql.h"
#include "OrdreFabricationDaoSql.h"
#include <QPlainTextEdit>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QDialog, public RegisteredWindow
{
    Q_OBJECT

public:
    MainWindow(AsyncDatabaseAccess & db, MainWindowV windowParams, QWidget *parent = nullptr);
    ~MainWindow();

    void ChangeState(int state);
    AsyncDatabaseAccess& db;
private:
    Ui::MainWindow *ui;
    QPixmap qrscan_ok;
    QPixmap qrscan_erreur;
    std::unique_ptr<TestDao> testDbAccess;
    std::unique_ptr<CarteElecDao> carteDbAccess;
    std::unique_ptr<FicheRecetteDao> ficheDbAccess;
    std::unique_ptr<UserDaoSql> userDbAccess;
    std::unique_ptr<OrdreFabricationDao> ofDbAccess;
    QString userTrigramme;
    OF ofFound;
    QMenu *gestion;
    bool isAdmin;
    void fetchCard();
    void checkCard();
    void checkFiche(const QString &pn);
    void checkOf(const QString &of);
    void checkUserAbilities(const int &ficheId, const bool &isConcerningTutor);
    void displayTutorRequest(const int &ficheId, const bool &isConcerningTutor);
    void scanDialog();
    static QRegularExpression regex;

public slots:
    void DisplayJsonEditor();
    void DisplayUserEditor();
    void remplirCasesInterface(const QString& PN, const QString& SN);
    void lectureQr(const QString& codeQr);
private:
    signals:
    void hideErrorLabel();
    void changeErrorLabel(QString text);
    void setPnField(QString pn);
    void changeFicheStatus(QString status, QString name, QString date, QString auteur);
    void requestUserAbilities(int ficheId, bool isConcerningTutor);
    void askForTutor(int ficheId, bool isConcerningTutor);
    void openMainProduction(int ficheId);

private slots:
    void on_pushButton_pressed();
    void on_pushButtonScan_clicked();
};
#endif // MAINWINDOW_H
