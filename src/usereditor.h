#ifndef USEREDITOR_H
#define USEREDITOR_H

#include "AsyncDatabaseAccess.h"
#include "UserDao.h"
#include "qdialog.h"
#include "usereditorwidget.h"
#include <QWidget>

namespace Ui {
class UserEditor;
}

class UserEditor : public QDialog
{
    Q_OBJECT

public:
    explicit UserEditor(AsyncDatabaseAccess & db, QString userTrigramme,QWidget *parent = nullptr);
    ~UserEditor();

public slots:
    void setUserList(QList<User>);
    void openParameters(const User &user);
    void resetUserList();
    void newUser();

signals:
    void userListSent(QList<User> list);
private:
    QList<UserEditorWidget*> listWidgets;
    Ui::UserEditor *ui;
    std::unique_ptr<UserDao> userDbAccess;
    QString trigramme;
};

#endif // USEREDITOR_H
