#ifndef INPUTNEWFICHE_H
#define INPUTNEWFICHE_H

#include <QDialog>

namespace Ui {
class InputNewFiche;
}

class InputNewFiche : public QDialog
{
    Q_OBJECT

public:
    explicit InputNewFiche(QWidget *parent = nullptr);
    ~InputNewFiche();

    static bool GetFicheInfo(QWidget *parent,QString *name,QString *PN);

    Ui::InputNewFiche *ui;
};

#endif // INPUTNEWFICHE_H
