#include "OrdreFabricationDaoSql.h"

/*!
 * \fn OrdreFabricationDaoSql::OrdreFabricationDaoSql
 * \param ada
 */
OrdreFabricationDaoSql::OrdreFabricationDaoSql(AsyncDatabaseAccess& ada)
    : OrdreFabricationDao{ada}
{

}

/*!
 * \fn OrdreFabricationDaoSql::getOF
 * \brief Recherche un ordre de fabrication (OF) dans la base de données correspondant à l'\a ID (chaîne de caractères) fourni.
 * \param id_text L'ID de l'ordre de fabrication recherché.
 * \return Les informations de l'ordre de fabrication s'il y a correspondance, une erreur dans le cas contraire.
 */
QFuture<OFResult> OrdreFabricationDaoSql::getOF(const QString &id_text)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](QString id_text){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Ordre_Fabrication\" WHERE id_text=?");
            query.addBindValue(id_text);
            query.exec();
            OFResult res;
            res.error = NO_ERROR;
            if(query.next()){
                res.of.id_text = id_text;
                res.of.PN = query.value("pn").toString();
                res.of.nb_cartes = query.value("nb_cartes").toInt();
                res.of.nb_restant = query.value("nb_restant").toInt();
                return res;
            }
            else {
               
                res.error = INVALID_PN;
                return res;
            }
        }, id_text);
}

/*!
 * \fn OrdreFabricationDaoSql::addOF
 * \brief Ajoute un ordre de fabrication à la base de données.
 * \param of L'ordre de fabrication à ajouter à la base de données.
 * \return Rien si le processus d'ajout se déroule correctement, sinon une erreur dans le cas contraire (soit l'odre existe déjà, soit une erreur liée à la connexion à la base de données).
 */
QFuture<OrdreFabricationDao::Error> OrdreFabricationDaoSql::addOF(const OF &of)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](OF of){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Ordre_Fabrication\" WHERE id_text=?");
            query.addBindValue(of.id_text);
            query.exec();
            if(query.next()){
                return Error::OF_ALREADY_EXISTS;
            }
            else {
                QSqlQuery queryCreation;
                queryCreation.prepare("INSERT INTO Ordre_Fabrication (id_text, pn, nb_cartes, nb_restant) VALUES (:id_text, :pn, :nb_cartes, :nb_restant)");
                query.bindValue(":id_text", of.id_text);
                query.bindValue(":pn", of.PN);
                query.bindValue(":nb_cartes", of.nb_cartes);
                query.bindValue(":nb_restant",of.nb_restant);
                query.exec();
                if(query.lastError().isValid()){
                    return Error::SQL_ERROR;
                }
                return Error::NO_ERROR;
            }
        }, of);
}


/*!
 * \fn OrdreFabricationDaoSQl::removeOF
 * \brief Supprime l'ordre de fabrication de la base de données correspondant à l'\a ID (chaîne de caractères) fourni.
 * \param id_text L'ID de l'ordre de fabrication que l'on souhaite supprimer.
 * \return Rien si le processus de suppression se déroule correctement, sinon une erreur dans le cas contraire (soit l'ordre n'existe pas dans la base de données, soit une erreur liée à la connexion à la base de données).
 */
QFuture<OrdreFabricationDao::Error> OrdreFabricationDaoSql::removeOF(const QString &id_text)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](QString id_text){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Ordre_Fabrication\" WHERE id_text=?");
            query.addBindValue(id_text);
            query.exec();
            if(query.next()){
                QSqlQuery queryCreation(QSqlDatabase::database("main-db"));
                queryCreation.prepare("DELETE FROM main_db.\"Ordre_Fabrication\" WHERE id_text=?");
                queryCreation.addBindValue(id_text);
                queryCreation.exec();
                if(queryCreation.lastError().isValid()){
                    return (Error::SQL_ERROR);
                }
                return Error::NO_ERROR;
            }
            else {
                return (Error::INVALID_ID_TEXT);
            }
        }, id_text);
}

/*!
 * \fn OrdreFabricationDaoSql::modifyOF
 * \brief Modifie l'OF distant par rapport à celui local.
 * \param of L'OF que l'on souhaite voir modifier.
 * \return Rien si le processus de modification se déroule correctement, sinon une erreur dans le cas contraire.
 */
QFuture<OrdreFabricationDao::Error> OrdreFabricationDaoSql::modifyOF(const OF& of)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](OF of){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Ordre_Fabrication\" WHERE id_text=?");
            query.addBindValue(of.id_text);
            query.exec();
            if(!query.next()){
                return (Error::INVALID_ID_TEXT);
            }
            else {
                QSqlQuery queryModify(QSqlDatabase::database("main-db"));
                queryModify.prepare("UPDATE main_db.\"Ordre_Fabrication\" SET pn=?, nb_cartes=?, nb_restant=? WHERE id_text=?");
                queryModify.addBindValue(of.PN);
                queryModify.addBindValue(of.nb_cartes);
                queryModify.addBindValue(of.nb_restant);
                queryModify.addBindValue(of.id_text);
                queryModify.exec();
                if(queryModify.lastError().isValid()){
                    return (Error::SQL_ERROR);
                }
                return Error::NO_ERROR;
            }
        }, of);
}

/*!
 * \brief OrdreFabricationDaoImpl::getLastError
 * \deprecated
 */
QString OrdreFabricationDaoSql::getLastError()
{
    return lastError;
}
