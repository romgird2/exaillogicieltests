#ifndef STEPRECETTE_H
#define STEPRECETTE_H


#include <string>
#include <vector>
#include <QString>
class stepRecette
{
public:

    enum type_recette
    {
        texte,
        mesure,
        script
    };

    type_recette type;
    std::vector<double> valeurs; // values for the type
    double unit; // factor to standard
    QString unit_name;
    QString description;
    QString name; // for scripts
    std::vector<QString> references;


    static stepRecette NewEmpty()
    {
        stepRecette step;
        step.type = texte;
        step.description = "";
        return step;
    }

    stepRecette();
    QString typeToString() const;
    void toMessageBox(const int &numero = -1) const;
};

#endif // STEPRECETTE_H
