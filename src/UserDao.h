#ifndef USERDAO_H
#define USERDAO_H

#include <QObject>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include "AsyncDatabaseAccess.h"

struct User {
    QString name;
    int isAdmin;
};

struct UserResult;

class UserDao
{
public:
    explicit UserDao(AsyncDatabaseAccess& db);
    virtual ~UserDao();
    enum Error {
        INVALID_PASSWORD,
        INVALID_NAME,
        USER_ALREADY_EXISTS,
        SQL_ERROR,
        NO_ERROR
    };
    virtual QFuture<UserResult> getUser(const QString &name, const QString &password) = 0;
    virtual QFuture<QList<User>> getListUsers() = 0;
    virtual QFuture<Error> addUser(const User &user, const QString &password) = 0;
    virtual QFuture<Error> removeUser(const QString &name) = 0;
    virtual QFuture<bool> isAdmin(const QString &name) = 0;
    virtual QString getLastError() = 0;
    virtual QFuture<Error> alterUser(const User &user) = 0;


protected:
    QString lastError;
    AsyncDatabaseAccess& db;

};

struct UserResult {
    User user;
    UserDao::Error error;
};

#endif // USERDAO_H
