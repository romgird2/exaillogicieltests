#include "RegisteredWindow.h"

/*!
 * \fn RegisteredWindow::managerReturn
 * \brief Renvoie le code de retour d'une fenêtre inscrite dans le gestionnaire de fenêtres, qui décrit la fênêtre à ouvrir après la fermeture de la fenêtre actuelle avec ses paramètres d'ouverture.
 * \return
 */
WindowManagerReturn RegisteredWindow::managerReturn()
{
    return returnCode;
}
