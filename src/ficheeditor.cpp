#include "ficheeditor.h"
#include "recette.h"
#include "ui_ficheeditor.h"
#include <QInputDialog>

/*!
 * \fn FicheEditor::setJson
 * \brief Créé la recette à partir du json fourni et créé toutes les étapes associées.
 * \param json Le JSON décrivant la fiche recette et ses étapes.
 */
void FicheEditor::setJson(const QString &json)
{

    Recette recette(json);
    for(stepRecette &step : recette.steps)
    {
        AddNewStep(step);
    }
}

/*!
 * \fn FicheEditor::FicheEditor
 * \brief Initialise l'éditeur d'une fiche recette.
 * \param name Nom de la fenêtre.
 * \param userTrigramme Trigramme de l'utilisateur.
 * \param ficheDbAccess Variable qui fait référence à la table "Fiche_Recette" de la base de données.
 * \param PN Le PN associé à la fiche recette
 * \param id L'ID de la fiche recette.
 * \param parent
 */
FicheEditor::FicheEditor(QString name,QString userTrigramme,FicheRecetteDao &ficheDbAccess, QString PN, int id, QWidget *parent) :
    QDialog(parent, Qt::Window),
    ui(new Ui::FicheEditor),
    steps(),
    ficheDbAccess(ficheDbAccess),
    currentFocus(nullptr)
{
    ui->setupUi(this);
    ui->scrollAreaWidgetContents->setLayout(new QVBoxLayout());
    this->userTrigramme = userTrigramme;
    this->PN = PN;
    this->id = id;
    this->setWindowTitle(name);
    connect(this, &FicheEditor::jsonReceived, this, &FicheEditor::setJson);
    connect(this, &FicheEditor::ficheSaved, this, &FicheEditor::notifySave);
    if(name != "")
        ficheDbAccess.getJsonRecette(id).then([this](QString json){emit jsonReceived(json);});
    connect(ui->newButton,&QPushButton::pressed,this,&FicheEditor::CreateNewStep);
    connect(ui->deleteButton,SIGNAL(pressed()),this,SLOT(DeleteCurrentStep()));
    connect(ui->copyButton,SIGNAL(pressed()),this,SLOT(CopyCurrentStep()));
    connect(ui->saveButton,SIGNAL(pressed()),this,SLOT(Save()));
    connect(ui->saveAsButton,SIGNAL(pressed()),this,SLOT(SaveAs()));

}

/*!
 * \fn FicheEditor::ChangeFocus
 * \brief Permet de sélectionner l'une des étapes.
 * \param src L'étape qui doit être sélectionnée.
 */
void FicheEditor::ChangeFocus(StepFicheWidget *src)
{
    if(currentFocus != nullptr)
        currentFocus->setFocus(false);
    currentFocus = src;
    currentFocus->setFocus(true);
}

/*!
 * \fn FicheEditor::AddNewStep
 * \brief Traite l'ajout d'une étape à la fiche recette par l'éditeur.
 * \param step L'étape à ajouter.
 */

void FicheEditor::AddNewStep(const stepRecette &step)
{
    steps.append(new StepFicheWidget(step,ui->scrollAreaWidgetContents));
    ui->scrollAreaWidgetContents->layout()->addWidget(steps.last());
    steps.last()->setVisible(true);
    connect(steps.last(),SIGNAL(pressed(StepFicheWidget*)),this,SLOT(ChangeFocus(StepFicheWidget*)));
    ChangeFocus(steps.last());
}

/*!
 * \fn FicheEditor::CreateNewStep
 * \brief Ajoute une étape vide à la fiche recette.
 */
void FicheEditor::CreateNewStep()
{
    AddNewStep(stepRecette::NewEmpty());

}

/*!
 * \fn FicheEditor::DeleteCurrentStep
 * \brief Supprime l'étape qui est sélectionnée.
 */
void FicheEditor::DeleteCurrentStep()
{
    if(currentFocus != nullptr)
    {
        currentFocus->deleteLater();
        int index = steps.indexOf(currentFocus);
        steps.remove(index);
        if(steps.length() > index)
            currentFocus = steps[index];
        else if(steps.length() > index-1)
            currentFocus = steps[index-1];
        else
            currentFocus = nullptr;
        ui->scrollAreaWidgetContents->layout()->removeWidget(currentFocus);
    }

}

/*!
 * \fn FicheEditor::CopyCurrentStep
 * \brief Créé une copie de l'étape qui est sélectionnée.
 */
void FicheEditor::CopyCurrentStep()
{
    if(currentFocus != nullptr)
        AddNewStep(currentFocus->content);
    else
        AddNewStep(steps.last()->content);
}

/*!
 * \fn FicheEditor::ToJson
 * \brief Transforme les étapes recensées dans l'éditeur en format JSON adapté.
 * \return Le JSON des étapes recensées dans l'éditeur.
 */
QString FicheEditor::ToJson()
{
    Recette recette("");
    recette.PN = PN;
    recette.titre = this->windowTitle();
    for(StepFicheWidget *fiches : steps)
    {
        QString error;
        if(!fiches->Pull(&error))
        {
            QMessageBox(QMessageBox::Warning,"Erreur lors de la sauvegarde",error,QMessageBox::Ok,this).exec();
            return "";
        }
        recette.AddStep(fiches->content);
    }
    return recette.toJson();
}

/*!
 * \fn FicheEditor::Save
 * \brief Enregistre les informations de la fiche recette dans la base de données.
 */
void FicheEditor::Save()
{
    QString json = FicheEditor::ToJson();
    if(json == "")
        return;
    FicheRecette fiche;
    fiche.id = this->id;
    fiche.PN = this->PN;
    fiche.name = this->windowTitle();
    fiche.Trigramme = userTrigramme;
    fiche.json = json;
    fiche.Date = QDate::currentDate();
    ficheDbAccess.addFicheRecette(fiche).then([this](FicheRecetteDao::Error res){
        if (res == FicheRecetteDao::Error::NO_ERROR)
            emit ficheSaved();
    });
}

/*!
 * \fn FicheEditor::SaveAs
 * \brief Enregistre sous un nom demandé les informations de la fiche recette dans la base de données.
 */
void FicheEditor::SaveAs()
{
    bool ok;
    QString text = QInputDialog::getText(this,"Nom de la fiche recette","Nom de la fiche recette:",QLineEdit::Normal,"",&ok);
    if(ok && !text.isEmpty())
    {
        this->setWindowTitle(text),
            Save();
    }
}

/*!
 * \fn FicheEditor::setProperties
 * \brief Met en place les paramètres de l'éditeur : titre de la fenêtre et PN.
 * \param name Nom souhaité pour la fenêtre.
 * \param PN PN inscrit dans l'éditeur.
 */
void FicheEditor::setProperties(const QString &name, const QString &PN)
{
    this->setWindowTitle(name);
    this->PN = PN;
}

/*!
 * \fn FicheEditor::notifySave
 * \brief Confirme que la sauvegarde s'est bien passée.
 */
void FicheEditor::notifySave() {
    QMessageBox::information(nullptr, "Sauvegarde", "La fiche a bien été sauvegardée");
}

/*!
 * \fn FicheEditor::~FicheEditor
 * \brief Destructeur de l'éditeur de fiche recette.
 */
FicheEditor::~FicheEditor()
{
    delete ui;
}
