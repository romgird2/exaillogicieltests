#ifndef WINDOWMANAGERTYPES_H
#define WINDOWMANAGERTYPES_H

#include "OrdreFabricationDao.h"
#include <variant>
#include <QString>

/*!
 * Structure renvoyée par une fenêtre souhaitant ouvrir un ConnexionDialog après sa fermeture.
*/
struct ConnexionDialogV{};
/*!
 * Structure renvoyée par une fenêtre souhaitant ouvrir MainWindow après sa fermeture.
*/
struct MainWindowV{
    int dialogStatus; // Le statut du dialogue de connexion
    QString trigramme; // Le trigramme de l'utilisateur connecté
    bool isAdmin; // L'utilisateur connecté est-il administrateur ?
    QString pn_field = ""; // Quel PN est actuellement en cours de production ? (PN vide = pas de production en cours)
    QString of_field = ""; // Quel OF est actuellement en cours de production ? (OF vide = pas de production en cours)
};
/*!
 * Structure renvoyée par une fenêtre souhaitant ouvrir les paramètres après sa fermeture.
*/
struct ParametersV{};
/*!
 * Structure renvoyée par une fenêtre souhaitant ouvrir l'éditeur de fiches recettes après sa fermeture.
*/
struct JsonEditorV{
    QString trigramme = "";
};
/*!
 * Structure renvoyée par une fenêtre souhaitant ouvrir la fenêtre de production après sa fermeture.
*/
struct MainProductionV{
    QString trigramme;
    bool isAdmin;
    int fiche_id;
    QString pn;
    OF of;
    QString sn;
};
/*!
 * Structure renvoyée par une fenêtre souhaitant ouvrir la fenêtre de statistiques après sa fermeture.
*/
struct InformationTestsV{};
/*!
 * Structure renvoyée par une fenêtre souhaitant fermer le logiciel après sa fermeture.
*/
struct ExitV{};

/*!
 * Type variant renvoyé par chaque fenêtre inscrite dans le gestionnaire de fenêtres.
*/
using WindowManagerReturn = std::variant<ConnexionDialogV,
                                         MainWindowV,
                                         ParametersV,
                                         JsonEditorV,
                                         MainProductionV,
                                         ExitV,
                                         InformationTestsV>;

#endif // WINDOWMANAGERTYPES_H
