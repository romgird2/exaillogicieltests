#ifndef REGISTEREDWINDOW_H
#define REGISTEREDWINDOW_H

#include "WindowManagerTypes.h"

class RegisteredWindow
{
public:
    virtual WindowManagerReturn managerReturn();
protected:
    WindowManagerReturn returnCode = ExitV();
};

#endif // REGISTEREDWINDOW_H
