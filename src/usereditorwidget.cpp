#include "usereditorwidget.h"
#include "ui_usereditorwidget.h"
#include "usereditorparameters.h"

/*!
 * \fn UserEditorWidget::UserEditorWidget
 * \brief Constructeur du widget recensant les informations relatives à un utilisateur.
 * \param user L'utilisateur concerné par le widget.
 * \param parent
 */
UserEditorWidget::UserEditorWidget(User user,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserEditorWidget)
{
    ui->setupUi(this);
    this->user = user;
    if(!user.isAdmin)
        ui->admin->setText("Utilisateur");
    ui->label->setText(user.name);
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(OpenParameters()));
}

/*!
 * \fn UserEditorWidget::OpenParameters
 * \brief Gère l'ouverture des paramètres d'un utilisateur à partir du widget.
 */
void UserEditorWidget::OpenParameters()
{
    emit OpeningParameters(user);
}

/*!
 * \fn UserEditorWidget::~UserEditorWidget
 * \brief Destructeur du widget recensant les informations relatives à un utilisateur.
 */
UserEditorWidget::~UserEditorWidget()
{
    delete ui;
}
