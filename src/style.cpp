#include "style.h"
#include "qtextdocument.h"
#include <QFileDialog>
#include <QPrinter>

/*!
 * \fn Style::putStyle
 * \brief Modifie le visuel d'un élément donné pour correspondre à la direction artistique.
 * \param target L'élement que l'on souhaite styliser.
 */
void Style::putStyle(QWidget *target)
{
    QFile *file;
    if(nuit)
        file = new QFile(":/style/exail.qss");
    else
        file = new QFile(":/style/exail_white.qss");
    file->open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file->readAll());
    target->setStyleSheet(styleSheet);
    file->close();
    target->ensurePolished();
}

/*!
 * \fn Style::GeneratePDF
 * \brief Génère un PDF stylisé.
 * \param data Les informations composant le PDF généré
 * \param parent
 */
void Style::GeneratePDF(std::shared_ptr<ResultMResult> data,QWidget *parent)
{
    QString directory = QFileDialog::getSaveFileName(parent,"Sauvegarde du pdf",QDir::homePath(),"PDF File (*.pdf)");
    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(directory);

    int incr = 0;

    QMap<int,QList<ResultM>> steps;

    for(ResultM result : data->res_m)
    {
        int index = result.pos_in_fiche;
        if(!steps.contains(index))
        {
            steps[index] = QList<ResultM>();
        }
        steps[index].push_back(result);
    }

    QTextDocument doc;

    QString html = QString("<center><font size=\"6\"><table border=\"1\"><thead><tr><th></th><th>Valeurs</th><th>Min</th><th>Max</th><th>Moyenne</th>")
                   +QString("<th>OK</th><th>NOK</th></tr></thead><tbody>");

    QList<int> indexes = steps.keys();
    std::sort(indexes.begin(),indexes.end());



    for(int index : indexes)
    {
        html += "<tr><td>Étape " + QString::number(index) + "</td>";

        QList<double> values;

        for(ResultM value : steps[index])
        {
            values.append(value.mesure);
        }


        double minimum = values[0];
        double maximum = values[0];

        double mean = 0;
        int nok_count = 0;
        int ok_count = 0;
        for(double value : values)
        {
            if(value < minimum){
                minimum = value;
            }
            if(value > maximum){
                maximum = value;
            }

            if(steps[index][0].min_value > value || value > steps[index][0].max_value){
                nok_count++;
            }
            else {
                ok_count++;
            }
            mean += value;
        }
        mean /= values.size();


        html += "<td>" + QString::number(values.size()) + "</td>";
        html += "<td>" + QString::number(minimum).mid(0,7) + "</td>";
        html += "<td>" + QString::number(maximum).mid(0,7) + "</td>";
        html += "<td>" + QString::number(mean).mid(0,7) + "</td>";
        html += "<td>" + QString::number(ok_count) + "</td>";
        html += "<td>" + QString::number(nok_count) + "</td>";


        html += "</tr>";
    }

    html += "</tbody>\n</table></font></center>";
    doc.setHtml(html);
    doc.setPageSize(printer.pageRect(QPrinter::Unit::Point).size()); // This is necessary if you want to hide the page number
    doc.print(&printer);

}
