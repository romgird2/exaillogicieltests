#include "stepfichewidget.h"
#include "recette.h"
#include "ui_stepfichewidget.h"

/*!
 * \fn StepFicheWidget::StepFicheWidget
 * \brief Constructeur du widget représentant une fiche recette.
 * \param content Contenu utilisé par le widget.
 * \param parent
 */
StepFicheWidget::StepFicheWidget(stepRecette content,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StepFicheWidget)
{
    ui->setupUi(this);

    this->content = content;
    Push();
    Actualise();
    connect(ui->typeBox,SIGNAL(currentIndexChanged(int)),this,SLOT(ChangeType(int)));
    ui->typeBox->setFocusPolicy( Qt::StrongFocus );
}

/*!
 * \fn StepFicheWidget::Actualise
 * \brief Actualise les informations affichées dans le widget de la fiche recette.
 */
void StepFicheWidget::Actualise()
{
    ui->mode_texte->setVisible(false);
    ui->mode_mesure->setVisible(false);
    ui->mode_script->setVisible(false);
    switch(ui->typeBox->currentIndex())
    {
    case 0:
        ui->mode_texte->setVisible(true);
        break;
    case 1:
        ui->mode_mesure->setVisible(true);
        break;
    case 2:
        ui->mode_script->setVisible(true);
        break;
    }
}

/*!
 * \fn StepFicheWidget::ChangeType
 * \brief Change le type d'une étape d'index donné.
 * \param index Index de l'étape dont on souhaite changer le type.
 */
void StepFicheWidget::ChangeType(int index)
{
    content = stepRecette::NewEmpty();
    switch(index)
    {
    case 0:
        content.type = stepRecette::type_recette::texte;
        content.description = "Texte qui sera affiché";
            break;
    case 1:
        content.type = stepRecette::type_recette::mesure;
        break;
    case 2:
        content.type = stepRecette::type_recette::script;
        content.name = "script.py";
        break;
    }
    Push();
    Actualise();
}

/*!
 * \fn StepFicheWidget::Push
 * \brief Convertis les données du JSON en informations utilisables pour le widget.
 */
void StepFicheWidget::Push()
{
    ui->typeBox->setCurrentIndex(content.type == stepRecette::type_recette::texte ? 0 : (content.type == stepRecette::type_recette::mesure ? 1 : 2));
    switch(content.type)
    {
    case stepRecette::type_recette::texte:
        ui->mode_texte_texte->setText(content.description);
        break;
    case stepRecette::type_recette::mesure:
    {
        ui->mode_mesure_combo_prefix->clear();
        ui->mode_mesure_combo_prefix->addItem("");
        for(int i = 0;i != Recette::nb_unit_factor;++i)
        {
            ui->mode_mesure_combo_prefix->addItem(Recette::unit_factor_name[i]);
        }
        for(int i = 0;i != Recette::nb_unit;++i)
        {
            ui->mode_mesure_combo_unit->addItem(Recette::unit_name[i]);
        }

        for(int i = 0;i != Recette::nb_unit_factor;++i)
        {
            if(Recette::unit_factor[i] == content.unit)
                ui->mode_mesure_combo_prefix->setCurrentIndex(i);
        }
        for(int i = 0;i != Recette::nb_unit;++i)
        {
            if(Recette::unit_name[i] == content.unit_name)
                ui->mode_mesure_combo_unit->setCurrentIndex(i);
        }

        QString valeurs;
        for(double valeur : content.valeurs)
        {
            valeurs += QString::number(valeur) + "\n";
        }
        ui->mode_mesure_valeurs->setText(valeurs);
        QString points;
        for(QString point : content.references)
        {
            points += point + "\n";
        }
        ui->mode_mesure_points->setText(points);
        break;
    }
    case stepRecette::type_recette::script:
        ui->mode_script_texte->setText(content.name);
        break;
    }
}

/*!
 * \fn StepFicheWidget::Pull
 * \brief Convertis les données du widget en un JSON.
 * \param error
 * \return Le JSON contenant les informations du widget.
 */
bool StepFicheWidget::Pull(QString *error)
{
    content.type = (stepRecette::type_recette)ui->typeBox->currentIndex();
    switch(content.type)
    {
    case stepRecette::type_recette::texte:
        content.description = ui->mode_texte_texte->toPlainText();
        break;
    case stepRecette::type_recette::mesure:
    {
        if(ui->mode_mesure_combo_prefix->currentIndex()==0)
            content.unit = 1;
        else
            content.unit = Recette::unit_factor[ui->mode_mesure_combo_prefix->currentIndex()-1];
        content.unit_name = Recette::unit_name[ui->mode_mesure_combo_unit->currentIndex()];

        QString valeurs;
        for(double valeur : content.valeurs)
        {
            valeurs += QString::number(valeur) + "\n";
        }

        content.valeurs.clear();
        for(QString line : ui->mode_mesure_valeurs->toPlainText().split("\n"))
        {
            if(line == "")
                continue;
            bool ok;
            content.valeurs.push_back(line.toDouble(&ok));
            if(!ok)
            {
                *error = "Valeur non conforme: " + line;
                return false;
            }
        }

        content.references.clear();
        for(QString line : ui->mode_mesure_points->toPlainText().split("\n"))
        {
            content.references.push_back(line);
        }
        break;
    }
    case stepRecette::type_recette::script:
        content.name = ui->mode_script_texte->text();
        break;
    }
    return true;
}

/*!
 * \fn StepFicheWidget::mousePressEvent
 * \brief Listener qui détecte un clic.
 * \param event L'event que l'on souhaite écouter.
 */
void StepFicheWidget::mousePressEvent(QMouseEvent * event)
{
    emit pressed(this);
}

/*!
 * \fn StepFicheWidget::setFocus
 * \brief Permet de modifier le style d'un élément que l'utilisateur a sélectionné.
 * \param focus
 */
void StepFicheWidget::setFocus(const bool &focus)
{
    if(focus){
        ui->content->setStyleSheet("*{background-color:#333333;}");
    }
    else{
        ui->content->setStyleSheet("");
    }
}

/*!
 * \fn StepFicheWidget::~StepFicheWidget
 * \brief Destructeur du widget représentant une fiche recette.
 */
StepFicheWidget::~StepFicheWidget()
{
    delete ui;
}
