#include "FicheRecetteDaoSql.h"

/*!
 * \fn FicheRecetteDaoSql::FicheRecetteDaoImpl
 * \param ada
 */
FicheRecetteDaoSql::FicheRecetteDaoSql(AsyncDatabaseAccess& ada)
    :FicheRecetteDao{ada}

{

}

/*!
 * \fn FicheRecetteDaoSql::getFicheRecette
 * \brief Recherche la fiche recette dans la base de données correspondant à l'\a ID fourni.
 * \param id ID de la fiche recette recherchée.
 * \return La fiche recette s'il y a correspondance, une erreur dans le cas contraire.
 */
QFuture<FicheRecetteResult> FicheRecetteDaoSql::getFicheRecette(const int &id)
{
 return QtConcurrent::run(db.getThreadPoolPtr(), [](int id){
        QSqlQuery query(QSqlDatabase::database("main-db"));
        query.prepare("SELECT * FROM main_db.\"Fiche_Recette\" WHERE id=?");;
        query.addBindValue(id);
        query.exec();
        FicheRecetteResult res;
        res.error = NO_ERROR;
        if(query.next()){
            res.fiche.id = query.value("id").toInt();

            return res;
        }
            else {
                res.error = INVALID_NAME;
                return res;
            }
        }, id);

}

/*!
 * \fn FicheRecetteDaoSql::getLastFicheRecette
 * \brief Recherche la dernière fiche recette associée au \a PN fourni.
 * \param pn PN de la fiche recette recherchée.
 * \return La fiche recette associée si le processus de recherche se déroule correctmeent, sinon une erreur dans le cas contraire.
 */
QFuture<FicheRecetteResult> FicheRecetteDaoSql::getLastFicheRecette(const QString &pn)
{
 return QtConcurrent::run(db.getThreadPoolPtr(), [](QString pn){
         QSqlQuery query(QSqlDatabase::database("main-db"));
         query.prepare("SELECT * FROM main_db.\"Fiche_Recette\" WHERE pn = ? ORDER BY date DESC");
         query.addBindValue(pn);
         query.exec();
         FicheRecetteResult res;
         res.error = NO_ERROR;
         if(query.next()){
             res.fiche.id = query.value("id").toInt();
             res.fiche.name = query.value("nom").toString();
             res.fiche.Date = query.value("date").toDate();
             res.fiche.PN = query.value("pn").toString();
             res.fiche.Trigramme = query.value("utilisateur").toString();
             res.fiche.json = query.value("json").toString();
             return res;
         }
         else {
            
             res.error = INVALID_PN;
             return res;
         }
     }, pn);

}


/*!
 * \fn FicheRecetteDaoSql::removeFicheRecette
 * \brief Supprime la fiche recette de la base de données correspondant à l'\a ID fourni.
 * \param id ID de la fiche recette que l'on souhaite supprimer.
 * \return Rien si le processus de suppression se déroule correctement, sinon une erreur dans le cas contraire (soit la fiche recette n'existe pas dans la base de données, soit une erreur liée à la connexion à la base de données)
 */
QFuture<FicheRecetteDao::Error> FicheRecetteDaoSql::removeFicheRecette(const int &id)
{
  return QtConcurrent::run(db.getThreadPoolPtr(), [](int id){
     QSqlQuery queryCheck(QSqlDatabase::database("main-db"));
     queryCheck.prepare("SELECT * FROM main_db.\"Fiche_Recette\" WHERE id=:id");
     queryCheck.bindValue(":id", id);
     queryCheck.exec();

     if (queryCheck.next()) {
         QSqlQuery queryDelete(QSqlDatabase::database("main-db"));
         queryDelete.prepare("DELETE FROM main_db.\"Fiche_Recette\" WHERE id=:id");
         queryDelete.bindValue(":id", id);
         if (!queryDelete.exec()) {
             return FicheRecetteDao::Error::SQL_ERROR;
         }
     } else {
         return FicheRecetteDao::Error::INVALID_NAME;
     }
     return FicheRecetteDao::Error::NO_ERROR;
 }, id);
}

/*!
 * \fn FicheRecetteDaoSql::addFicheRecette
 * \brief Ajoute une fiche recette à la base de données.
 * \param fiche La carte électronique à ajouter à la base de données.
 * \return Rien si le processus d'ajout se déroule correctement, sinon une erreur dans le cas contraire (soit la fiche recette existe déjà, soit une erreur liée à la connexion à la base de données).
 */
QFuture<FicheRecetteDao::Error> FicheRecetteDaoSql::addFicheRecette(const FicheRecette &fiche)
{
 return QtConcurrent::run(db.getThreadPoolPtr(), [](FicheRecette fiche){
        QSqlQuery queryInsert(QSqlDatabase::database("main-db"));
        queryInsert.prepare("INSERT INTO main_db.\"Fiche_Recette\" (nom, date, pn, utilisateur, json) VALUES (?, ?, ?, ?, ?)");
        queryInsert.addBindValue(fiche.name);
        queryInsert.addBindValue(fiche.Date);
        queryInsert.addBindValue(fiche.PN);
        queryInsert.addBindValue(fiche.Trigramme);
        queryInsert.addBindValue(fiche.json);

        if (!queryInsert.exec()) {
            return FicheRecetteDao::Error::SQL_ERROR;
        }
        return FicheRecetteDao::Error::NO_ERROR;
 }, fiche);
}

/*!
 * \fn FicheRecetteDaoSql::getLastError
 * \deprecated
 */
QString FicheRecetteDaoSql::getLastError()
{
 return lastError;
}

/*!
 * \fn FicheRecetteDaoImpl::getListFichesRecettes
 * \brief Renvoie la liste des fiches recettes stockées dans la base de données.
 * \return La liste des fiches recettes stockées dans la base de données.
 */
QFuture<QList<FicheRecetteResult>> FicheRecetteDaoSql::getListFichesRecettes()
{
 return QtConcurrent::run(db.getThreadPoolPtr(), [](){
     QSqlQuery query(QSqlDatabase::database("main-db"));
     query.prepare("SELECT * FROM main_db.\"Fiche_Recette\"");
     query.exec();
     QList<FicheRecetteResult> res;

     while(query.next()){
         FicheRecetteResult fiche;
         fiche.fiche.id = query.value("id").toInt();
         fiche.fiche.name = query.value("nom").toString();
         fiche.fiche.PN = query.value("PN").toString();
         fiche.fiche.Trigramme = query.value("utilisateur").toString();
         fiche.fiche.Date = query.value("date").toDate();
         fiche.fiche.json = query.value("json").toString();
         res.push_back(fiche);
     }


     return res;
 });
}

/*!
 * \fn FicheRecetteDaoSql::getJsonRecette
 * \brief Recherche le contenu JSON associé à la fiche recette de nom donnée.
 * \param name Le nom de la fiche recette qui contient le contenu JSON.
 * \return Le contenu JSON associée à la fiche recette de nom donnée.
 */
QFuture<QString> FicheRecetteDaoSql::getJsonRecette(const QString &name)
{
 return QtConcurrent::run(db.getThreadPoolPtr(), [](QString name){
         QSqlQuery query(QSqlDatabase::database("main-db"));
         query.prepare("SELECT * FROM main_db.\"Fiche_Recette\" WHERE nom=?");
         query.addBindValue(name);
         query.exec();
         if(query.next())
         {
             return query.value("json").toString();
         }
         else {
             return QString("");
         }
     }, name);
}

/*!
 * \fn FicheRecetteDaoSql::getJsonRecette
 * \brief Recherche le contenu JSON associé à la fiche recette de nom donnée.
 * \param id L'ID de la fiche recette qui contient le contenu JSON.
 * \return Le contenu JSON associée à la fiche recette de nom donnée.
 */
QFuture<QString> FicheRecetteDaoSql::getJsonRecette(const int &id)
{
 return QtConcurrent::run(db.getThreadPoolPtr(), [](int id){
         QSqlQuery query(QSqlDatabase::database("main-db"));
         query.prepare("SELECT * FROM main_db.\"Fiche_Recette\" WHERE id=?");
         query.addBindValue(id);
         query.exec();
         if(query.next())
         {
             return query.value("json").toString();
         }
         else {
             return QString("");
         }
     }, id);
}

/*!
 * \fn FicheRecetteDaoSql::alterFicheRecette
 * \brief Modifie la fiche recette distante par rapport à celle locale.
 * \param fiche La fiche recette que l'on souhaite voir modifier.
 * \return Rien si le processus de modification se déroule correctement, sinon une erreur dans le cas contraire.
 */
QFuture<FicheRecetteDaoSql::Error> FicheRecetteDaoSql::alterFicheRecette(const FicheRecette &fiche)
{
 return QtConcurrent::run(db.getThreadPoolPtr(), [](FicheRecette fiche){
         QSqlQuery queryInsert(QSqlDatabase::database("main-db"));
         queryInsert.prepare("UPDATE \"Fiche_Recette\" SET nom=?,date=?,pn=?,utilisateur=?,json=? WHERE id=?");
         queryInsert.addBindValue(fiche.name);
         queryInsert.addBindValue(fiche.Date);
         queryInsert.addBindValue(fiche.PN);
         queryInsert.addBindValue(fiche.Trigramme);
         queryInsert.addBindValue(fiche.json);
         queryInsert.addBindValue(fiche.id);

         if (!queryInsert.exec()) {
             return FicheRecetteDao::Error::SQL_ERROR;
         }
         return FicheRecetteDao::Error::NO_ERROR;
     }, fiche);
}






















