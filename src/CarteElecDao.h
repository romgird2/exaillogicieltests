#ifndef CARTEELECDAO_H
#define CARTEELECDAO_H

#include <QObject>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include "AsyncDatabaseAccess.h"

struct CarteElec {
    QString SN;
    QString PN;
};

struct CarteElecResult;

class CarteElecDao
{
public:
    explicit CarteElecDao(AsyncDatabaseAccess& db);
    virtual ~CarteElecDao();
    enum Error {
        INVALID_PN,
        INVALID_SN,
        CARTE_ALREADY_EXISTS,
        SQL_ERROR,
        NO_ERROR
    };
    virtual QFuture<CarteElecResult> getCarteElec(const QString &SN) = 0;
    virtual QFuture<Error> addCarteElec(const CarteElec &carte_elec) = 0;
    virtual QFuture<Error> removeCarteElec(const QString &SN) = 0;
    virtual QString getLastError() = 0;

protected:
    QString lastError;
    AsyncDatabaseAccess& db;

};

struct CarteElecResult {
    CarteElec carte_elec;
    CarteElecDao::Error error;
};

#endif // CARTEELECDAO_H
