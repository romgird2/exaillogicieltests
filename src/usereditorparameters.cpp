#include "usereditorparameters.h"
#include "qmessagebox.h"
#include "ui_usereditorparameters.h"
#include <QInputDialog>

/*!
 * \fn UserEditorParameters::UserEditorParameters
 * \brief Constructeur de l'interface recensant les paramètres relaitfs à un utilisateur spécifié.
 * \param db La base de données.
 * \param trigramme Le trigramme de l'utilisateur.
 * \param user Le nom de l'utilisateur dont on souhaite afficher les paramètres.
 * \param parent
 */
UserEditorParameters::UserEditorParameters(UserDao &db,QString trigramme,User user,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserEditorParameters),
    userDbAccess(db),
    needUpdate(false)
{
    ui->setupUi(this);
    this->userTrigramme = trigramme;
    if(user.isAdmin)
        ui->checkAdministrateur->setCheckState(Qt::Checked);
    name = user.name;
    ui->groupBox->setTitle("Paramètres de l'utilisateur " + user.name);
    if(user.name == trigramme) // on va éviter de supprimer l'utilisateur actuel
    {
        ui->checkAdministrateur->setEnabled(false);
        ui->deleteUser->setEnabled(false);
    }
    connect(ui->save,SIGNAL(pressed()),this,SLOT(Save()));
    connect(ui->mdpReset,SIGNAL(pressed()),this,SLOT(ResetMdp()));
    connect(ui->deleteUser,SIGNAL(pressed()),this,SLOT(Delete()));
}

/*!
 * \fn UserEditorParameters::Save
 * \brief Enregistre les informations entrées dans l'interface.
 */
void UserEditorParameters::Save()
{
    needUpdate = true;
    User user;
    user.isAdmin = ui->checkAdministrateur->isChecked();
    user.name = name;
    userDbAccess.alterUser(user).then([this](UserDao::Error error){
    });
}

/*!
 * \fn UserEditorParameters::ResetMdp
 * \brief Gère la réinitialisation du mot de passe.
 */
void UserEditorParameters::ResetMdp()
{
    if(QMessageBox::Yes == QMessageBox(QMessageBox::Warning, "Mot de passe utilisateur", "Attention, voulez-vous reintialiser le mot de passe de l'utilisateur " + name + "?", QMessageBox::Yes|QMessageBox::No,this).exec())
    {
        QString text;bool ok;
        QString mdp1 = QInputDialog::getText(this,"Mot de passe","Nouveau mot de passe",QLineEdit::Password,text,&ok);
        if(ok && mdp1.size())
        {
            QString mdp2 = QInputDialog::getText(this,"Mot de passe","Veuillez réentrer le mot de passe",QLineEdit::Password,text,&ok);
            if(ok && mdp1==mdp2)
            {
                userDbAccess.removeUser(name);
                User user;
                user.isAdmin = ui->checkAdministrateur->isChecked();
                user.name = name;
                userDbAccess.addUser(user,mdp1);
                QMessageBox(QMessageBox::Information, "Réintialisation terminé", "Réintialisation terminé", QMessageBox::Ok,this).exec();
            }
            else if(ok)
            {
                QMessageBox(QMessageBox::Warning, "Réintialisation échoué", "Réintialisation échoué", QMessageBox::Ok,this).exec();
            }
            else
            {
                QMessageBox(QMessageBox::Warning, "Réintialisation avorté", "Réintialisation avorté", QMessageBox::Ok,this).exec();
            }
        }
        else if(ok)
        {
            QMessageBox(QMessageBox::Warning, "Réintialisation échoué", "Le mot de passe doit contenir au moins 3 caractères", QMessageBox::Ok,this).exec();
        }
        else
        {
            QMessageBox(QMessageBox::Warning, "Réintialisation avorté", "Réintialisation avorté", QMessageBox::Ok,this).exec();
        }


    }
}

/*!
 * \fn UserEditorParameters::Delete
 * \brief Gère la suppression d'un utilisateur.
 */
void UserEditorParameters::Delete()
{
    needUpdate = true;
    if(QMessageBox::Yes == QMessageBox(QMessageBox::Warning, "Suppression d'utilisateur", "Attention, voulez-vous supprimer l'utilisateur " + name + "?", QMessageBox::Yes|QMessageBox::No).exec()) {
        userDbAccess.removeUser(name);
        this->close();
    }
}

/*!
 * \fn UserEditorParameters::closeEvent
 * \brief Gère la fermeture de l'interface recensant les paramètres relaitfs à un utilisateur spécifié.
 * \param event
 */
void UserEditorParameters::closeEvent(QCloseEvent *event)
{
    if(needUpdate)
        emit UpdatingUserNeeded();
}

/*!
 * \fn UserEditorParameters::~UserEditorParameters
 * \brief Destructeur de l'interface recensant les paramètres relaitfs à un utilisateur spécifié.
 */
UserEditorParameters::~UserEditorParameters()
{
    delete ui;
}
