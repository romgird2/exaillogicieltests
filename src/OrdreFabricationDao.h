#ifndef ORDREFABRICATIONDAO_H
#define ORDREFABRICATIONDAO_H

#include <QObject>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include "AsyncDatabaseAccess.h"

struct OF {
    QString id_text;
    QString PN;
    int nb_cartes;
    int nb_restant;
};

struct OFResult;

class OrdreFabricationDao
{
public:
    explicit OrdreFabricationDao(AsyncDatabaseAccess& db);
    virtual ~OrdreFabricationDao();
    enum Error {
        INVALID_ID_TEXT,
        INVALID_PN,
        INVALID_NB_CARTES,
        OF_ALREADY_EXISTS,
        SQL_ERROR,
        NO_ERROR
    };
    virtual QFuture<OFResult> getOF(const QString &id_text) = 0;
    virtual QFuture<Error> addOF(const OF &of) = 0;
    virtual QFuture<Error> removeOF(const QString &id_text) = 0;
    virtual QFuture<Error> modifyOF(const OF& of) = 0;
    virtual QString getLastError() = 0;

    /*signals:
    void userFound(User user);
    void dataError(UserDao::Error error);*/

protected:
    QString lastError;
    AsyncDatabaseAccess& db;

};

struct OFResult {
    OF of;
    OrdreFabricationDao::Error error;
};

#endif // ORDREFABRICATIONDAO_H

