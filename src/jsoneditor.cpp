#include "jsoneditor.h"
#include "FicheRecetteDaoSql.h"
#include "ficheeditor.h"
#include "qmenu.h"
#include "ui_jsoneditor.h"
#include "style.h"
#include "inputnewfiche.h"
#include <iostream>

/*!
 * \fn jsonEditor::jsonEditor
 * \brief Constructeur de l'éditeur JSON.
 * \param db Base de données utilisée.
 * \param userTrigramme Trigramme de l'utilisateur.
 * \param parent
 */
jsonEditor::jsonEditor(AsyncDatabaseAccess &db,QString userTrigramme,QWidget *parent) :
    QDialog(parent, Qt::Window),
    ui(new Ui::jsonEditor)
{
    ui->setupUi(this);
    this->userTrigramme = userTrigramme;

    ficheDbAccess = std::make_unique<FicheRecetteDaoSql>(db);


    connect(ui->treeWidget,SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)),this,SLOT(openFicheEditor(QTreeWidgetItem*,int)));
    connect(this, &jsonEditor::ficheListSent, this, &jsonEditor::setFicheList);
    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->treeWidget,&QTreeWidget::customContextMenuRequested,this,&jsonEditor::showMenu);
    connect(ui->createNew,SIGNAL(clicked()),this,SLOT(createNew()));

    ficheDbAccess->getListFichesRecettes().then([this](QList<FicheRecetteResult> list){emit ficheListSent(list);});
}

/*!
 * \fn jsonEditor::openFicheEditor
 * \brief Permet l'ouverture de l'éditeur JSON.
 * \param item L'item sélectionné dans la liste.
 * \param x
 */
void jsonEditor::openFicheEditor(QTreeWidgetItem* item, const int &x)
{
    if(!item->parent())
        return;
    FicheEditor *ficheditor = new FicheEditor(item->text(0),userTrigramme,*ficheDbAccess,item->data(1,Qt::UserRole).toString(),item->data(0,Qt::UserRole).toInt(),this);
    ficheditor->show();
}

/*!
 * \fn jsonEditor::~jsonEditor
 * \brief Destructeur de l'éditeur JSON.
 */
jsonEditor::~jsonEditor()
{
    delete ui;
}

/*!
 * \fn jsonEditor::setFicheList
 * \brief Définit la liste des fiches recettes.
 * \param list Liste des fiches recettes souhaitées.
 */
void jsonEditor::setFicheList(const QList<FicheRecetteResult> &list){
    ui->treeWidget->clear();
    QList<QString> cartes;
    for(const FicheRecetteResult& fiche : list)
    {
        if(!cartes.contains(fiche.fiche.PN))
            cartes.append(fiche.fiche.PN);
    }
    for(QString& PN : cartes)
    {
        QTreeWidgetItem* tempItem = new QTreeWidgetItem();
        tempItem->setText(0,PN);
        ui->treeWidget->addTopLevelItem(tempItem);
        for(const FicheRecetteResult& fiche : list)
        {
            if(fiche.fiche.PN == PN)
            {
                QTreeWidgetItem* tempChildren = new QTreeWidgetItem(tempItem);
                tempChildren->setText(0,fiche.fiche.name);
                tempChildren->setText(1,fiche.fiche.Date.toString("dd/MM/yyyy"));
                tempChildren->setText(2,fiche.fiche.Trigramme);
                tempChildren->setData(0,Qt::UserRole,fiche.fiche.id);
                tempChildren->setData(1,Qt::UserRole,fiche.fiche.PN);
            }
        }
    }
    ui->treeWidget->expandAll();
    for(int i = 0;i < ui->treeWidget->columnCount();++i)
        ui->treeWidget->resizeColumnToContents(i);
}

/*!
 * \fn jsonEditor::showMenu
 * \brief Affiche le menu avec les actions.
 * \param pos Position du menu.
 */
void jsonEditor::showMenu(const QPoint & pos) {

    QMenu menu(this);
    menu.addAction(ui->actionOpen);
    menu.addAction(ui->actionDelete);

    ui->actionOpen->setData(QVariant(pos));
    ui->actionDelete->setData(QVariant(pos));

    menu.exec( ui->treeWidget->mapToGlobal(pos) );
}

/*!
 * \fn jsonEditor::on_actionOpen_triggered
 * \brief Définis les actions après activation des boutons "Ouvrir"
 */
void jsonEditor::on_actionOpen_triggered()
{
    QTreeWidgetItem *clickedItem = ui->treeWidget->itemAt(ui->actionOpen->data().toPoint());
    openFicheEditor(clickedItem, 0);
}

/*!
 * \fn jsonEditor::on_actionDelete_triggered
 * \brief Définis les actions après activation des boutons "Fermer"
 */
void jsonEditor::on_actionDelete_triggered()
{
    QTreeWidgetItem *clickedItem = ui->treeWidget->itemAt(ui->actionDelete->data().toPoint());
   
    ficheDbAccess->removeFicheRecette(clickedItem->data(0, Qt::UserRole).toInt());
    ficheDbAccess->getListFichesRecettes().then([this](QList<FicheRecetteResult> list){emit ficheListSent(list);});
}

/*!
 * \fn jsonEditor::createNew
 * \brief Gère la création d'une nouvelle fiche recette avec son édition.
 */
void jsonEditor::createNew()
{
    QString name,PN;
    bool ok = InputNewFiche::GetFicheInfo(this,&name,&PN);
    if(ok)
    {
        FicheEditor *ficheditor = new FicheEditor("",userTrigramme,*ficheDbAccess,0,0,this);
        ficheditor->setProperties(name,PN);
        ficheditor->show();
    }

}
