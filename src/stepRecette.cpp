#include "stepRecette.h"
#include <QMessagebox.h>

/*!
 * \fn stepRecette::stepRecette
 * \brief Constructeur d'une étape de la recette.
 */
stepRecette::stepRecette() : valeurs(),references()
{

}

/*!
 * \fn stepRecette::typeToString
 * \brief Convertis le type d'une étape en une suite de caractères.
 * \return La chaîne de caractère correspondant au type d'étape ("Description", "Mesure", "Script")
 */
QString stepRecette::typeToString() const {
    if(type == texte){
        return "Description";
    }
    else if(type == mesure){
        return "Mesure";
    }
    else if(type == script){
        return "Script";
    }
    return "";
}

/*!
 * \fn stepRecette::toMessageBox
 * \brief Génère une alerte contextuelle récapitulant les informations d'une étape.
 * \param numero
 */
void stepRecette::toMessageBox(const int &numero) const {
    QString text = typeToString() + "\r\n";
    if(type == texte) {
        text += description;
    }
    else if(type == mesure) {
        text += "Points : ";
        for(auto& p : this->references){
            text += p + " ";
        }
        text  += "\r\nValeurs limites ";
        text += (valeurs.size() > 0 ? QString::number(this->valeurs[0]) : "") + " ";
        text += (valeurs.size() > 1 ? QString::number(this->valeurs[1]) : "") + " ";
        text += unit_name;
    }
    else if(type == script){
        text += name;
    }
    QMessageBox::information(nullptr, numero > 0 ? QString("Etape %1").arg(numero) : typeToString(), text);
}
