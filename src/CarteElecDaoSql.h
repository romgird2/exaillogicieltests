#ifndef CARTEELECDAOSQL_H
#define CARTEELECDAOSQL_H

#include "CarteElecDao.h"

class CarteElecDaoSql : public CarteElecDao
{
public:
    explicit CarteElecDaoSql(AsyncDatabaseAccess& db);
    virtual QFuture<CarteElecResult> getCarteElec(const QString &SN);
    virtual QFuture<Error> addCarteElec(const CarteElec &carte_elec);
    virtual QFuture<Error> removeCarteElec(const QString &SN);
    virtual QString getLastError();
};

#endif // CARTEELECDAOSQL_H
