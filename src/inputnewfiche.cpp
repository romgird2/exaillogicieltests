#include "inputnewfiche.h"
#include "ui_inputnewfiche.h"

/*!
 * \fn InputNewFiche::InputNewFiche
 * \brief Constructeur de l'interface d'ajout d'une nouvelle fiche recette.
 * \param parent
 */
InputNewFiche::InputNewFiche(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InputNewFiche)
{
    ui->setupUi(this);
}

/*!
 * \fn InputNewFiche::~InputNewFiche
 * \brief Destructeur de l'interface d'ajout d'une nouvelle fiche recette.
 */
InputNewFiche::~InputNewFiche()
{
    delete ui;
}

/*!
 * \fn InputNewFiche::GetFicheInfo
 * \brief Vérifie que l'utilisateur veut bien créé une fiche recette avec le nom et le PN donné.
 * \param parent
 * \param name Nom souhaité pour la fiche recette.
 * \param PN PN souhaité pour la fiche recette.
 * \return True si l'utilisateur veut bien créé une fiche recette, False sinon.
 */
bool InputNewFiche::GetFicheInfo(QWidget *parent,QString *name,QString *PN)
{
    InputNewFiche newfiche(parent);
    if(newfiche.exec() == QDialog::Accepted)
    {
        *name = newfiche.ui->lineEdit->text();
        *PN = newfiche.ui->lineEdit_2->text();
        return true;
    }
    return false;


}
