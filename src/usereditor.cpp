#include "usereditor.h"
#include "UserDaoSql.h"
#include "newuser.h"
#include "ui_usereditor.h"
#include "usereditorparameters.h"
#include "usereditorwidget.h"

/*!
 * \fn UserEditor::UserEditor
 * \brief Constructeur de l'interface d'édition des utilisateurs.
 * \param db La base de données.
 * \param userTrigramme Le trigramme de l'utilisateur.
 * \param parent
 */
UserEditor::UserEditor(AsyncDatabaseAccess & db, QString userTrigramme,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserEditor),
    listWidgets()
{
    ui->setupUi(this);
    ui->content->setLayout(new QVBoxLayout());
    this->trigramme = userTrigramme;

    userDbAccess = std::make_unique<UserDaoSql>(db);

    connect(this, &UserEditor::userListSent, this, &UserEditor::setUserList);
    connect(ui->pushButton,SIGNAL(pressed()),this,SLOT(newUser()));

    userDbAccess->getListUsers().then([this](QList<User> list){emit userListSent(list);});
}

/*!
 * \fn UserEditor::setUserList
 * \brief Définis la liste des utilisateurs.
 * \param list La liste des utilisateurs.
 */
void UserEditor::setUserList(QList<User> list)
{
    for(User user : list)
    {
        UserEditorWidget *userWidget = new UserEditorWidget(user,ui->content);
        connect(userWidget,SIGNAL(OpeningParameters(User)),this,SLOT(openParameters(User)));
        userWidget->setVisible(true);
        ui->content->layout()->addWidget(userWidget);
        listWidgets.push_back(userWidget);
    }
}

/*!
 * \fn UserEditor::resetUserList
 * \brief Remet à zéro la liste des utilisateurs.
 */
void UserEditor::resetUserList()
{
    for(UserEditorWidget *target : listWidgets)
    {
        ui->content->layout()->removeWidget(target);
    }
    userDbAccess->getListUsers().then([this](QList<User> list){emit userListSent(list);});
}

/*!
 * \fn UserEditor::openParameters
 * \brief Ouvre l'interface des paramètres concernant un utilisateur spécifié.
 * \param user L'utilisateur dont on souhaite afficher les paramètres.
 */
void UserEditor::openParameters(const User &user)
{
    UserEditorParameters *para = new UserEditorParameters(*userDbAccess,trigramme,user,this);
    connect(para,SIGNAL(UpdatingUserNeeded()),this,SLOT(resetUserList()));
    para->show();
}

/*!
 * \fn UserEditor::newUser
 * \brief Gère la création d'un nouvel utilisateur.
 */
void UserEditor::newUser()
{
    NewUser *creation = new NewUser(*userDbAccess,this);
    connect(creation,SIGNAL(UpdatingUserNeeded()),this,SLOT(resetUserList()));
    creation->show();
}


/*!
 * \fn UserEditor::~UserEditor
 * \brief Destructeur de l'interface d'édition des utilisateurs.
 */
UserEditor::~UserEditor()
{
    delete ui;
}
