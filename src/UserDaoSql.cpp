#include "UserDaoSql.h"
#include "FicheRecetteDao.h"

/*!
 * \fn UserDaoSql::UserDaoImpl
 * \param ada
 */
UserDaoSql::UserDaoSql(AsyncDatabaseAccess& ada)
    : UserDao{ada}
{

}

/*!
 * \fn UserDaoSql::getUser
 * \brief Renvoie l'utilisateur associé au \a trigramme si le \a mot \a de \a passe en paramètre correspond au mot de passe associé dans la base de données.
 * \param name Le trigramme de l'utilisateur.
 * \param password Le mot de passe à vérifier.
 * \return Les informations de l'utilisateur associé au trigramme s'il y a correspondance des mots de passe, une erreur dans le cas contraire.
 */
QFuture<UserResult> UserDaoSql::getUser(const QString &name, const QString &password)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](QString name, QString password){
        QSqlQuery query(QSqlDatabase::database("main-db"));
    query.prepare("SELECT * FROM main_db.\"Utilisateur\" WHERE trigramme=?");
    query.addBindValue(name);
    query.exec();
    UserResult res;
    res.error = NO_ERROR;
    if(query.next()){
        if(query.value("mdp").toString() != QString(UserDaoSql::hashPassword(password, query.value("salt").toString()).toHex())){
            res.error = INVALID_PASSWORD;
            return res;
        }
        else {
            res.user.name = name;
            res.user.isAdmin = query.value("admin").toBool();
            return res;
        }
    }
    else {
    
        res.error = INVALID_NAME;
        return res;
    }
    }, name, password);
}

/*!
 * \fn UserDaoSql::getListUsers
 * \return Récupère la liste des utilisateurs depuis la base de données distante.
 */
QFuture<QList<User>> UserDaoSql::getListUsers()
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Utilisateur\"");
            query.exec();
            QList<User> list;
            while(query.next())
            {
                User user;
                user.name = query.value("trigramme").toString();
                user.isAdmin = query.value("admin").toBool();

                list.append(user);
            }
            return list;
        });
}

/*!
 * \fn UserDaoSql::addUser
 * \brief Ajoute un utilisateur à la base de données.
 * \param user Le trigramme de l'utilisateur à ajouter à la base de données.
 * \param password Le mot de passe de l'utilisateur à ajouter à la base de données.
 * \return Rien si le processus d'ajout se déroule correctement, sinon une erreur dans le cas contraire (soit l'utilisateur existe déjà, soit une erreur liée à la connexion à la base de données).
 */
QFuture<UserDao::Error> UserDaoSql::addUser(const User &user, const QString &password)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[this](User user, QString password){
       
    QSqlQuery query(QSqlDatabase::database("main-db"));
    query.prepare("SELECT * FROM main_db.\"Utilisateur\" WHERE trigramme=:name");
    query.bindValue(":name", user.name);
    query.exec();
    if(query.next()){
        return Error::USER_ALREADY_EXISTS;
    }
    else {
        QSqlQuery queryCreation(QSqlDatabase::database("main-db"));
        queryCreation.prepare("INSERT INTO main_db.\"Utilisateur\" (trigramme, mdp, admin, salt) VALUES (?, ?, ?, ?)");
        queryCreation.addBindValue(user.name);
        QString salt = QString::number(QRandomGenerator::global()->bounded(0,INT_MAX));
        auto tg = UserDaoSql::hashPassword(password, salt);
        queryCreation.addBindValue(QString(UserDaoSql::hashPassword(password, salt).toHex()));
        queryCreation.addBindValue((bool)user.isAdmin);
        queryCreation.addBindValue(salt);
       
        queryCreation.exec();
        if(queryCreation.lastError().isValid()){           
            return Error::SQL_ERROR;
        }
        
        return Error::NO_ERROR;
    }
    }, user, password);
}

/*!
 * \fn UserDaoSql::removeUser
 * \brief Supprime l'utilisateur de la base de données correspondant au \a trigramme fourni.
 * \param name Le trigramme de l'utilisateur à supprimer de la base de données.
 * \return Rien si le processus de suppression se déroule correctement, sinon une erreur dans le cas contraire (soit l'utilisateur n'existe pas dans la base de données, soit une erreur liée à la connexion à la base de données).
 */
QFuture<UserDao::Error> UserDaoSql::removeUser(const QString &name)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](QString name){
    QSqlQuery query(QSqlDatabase::database("main-db"));
    query.prepare("SELECT * FROM main_db.\"Utilisateur\" WHERE trigramme=?");
    query.addBindValue(name);
    query.exec();
    if(query.next()){
        QSqlQuery queryCreation(QSqlDatabase::database("main-db"));
        queryCreation.prepare("DELETE FROM main_db.\"Utilisateur\" WHERE trigramme=?");
        queryCreation.addBindValue(name);
        queryCreation.exec();
        if(queryCreation.lastError().isValid()){
            return (Error::SQL_ERROR);
        }
        return Error::NO_ERROR;
    }
    else {
        return (Error::INVALID_NAME);
    }
    }, name);
}

QFuture<UserDao::Error> UserDaoSql::alterUser(const User &user)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](User user){
            QSqlQuery queryVerify(QSqlDatabase::database("main-db"));
            queryVerify.prepare("SELECT * FROM main_db.\"Utilisateur\" WHERE trigramme=?");
            queryVerify.addBindValue(user.name);
            queryVerify.exec();
            if(queryVerify.next()){
                QSqlQuery queryAlter(QSqlDatabase::database("main-db"));
                queryAlter.prepare("UPDATE main_db.\"Utilisateur\" SET admin=? WHERE trigramme=?");
                queryAlter.addBindValue((bool)user.isAdmin);
                queryAlter.addBindValue(user.name);
                queryAlter.exec();
                if(queryAlter.lastError().isValid()){
                    return (Error::SQL_ERROR);
                }
                return Error::NO_ERROR;
            }
            else{
                return Error::INVALID_NAME;
            }
    }, user);
}

/*!
 * \fn UserDaoSql::isAdmin
 * \brief Vérifie si un utilisateur est un administrateur.
 * \param name Le trigramme de l'utilisateur que l'on souhaite vérifier.
 * \return True si l'utilisateur est un administrateur, sinon False.
 */
QFuture<bool> UserDaoSql::isAdmin(const QString &name) {

    return QtConcurrent::run(db.getThreadPoolPtr(),[](QString name){
    QSqlQuery query(QSqlDatabase::database("main-db"));
    query.prepare("SELECT * FROM main_db.\"Utilisateur\" WHERE trigramme=:name");
    query.bindValue(":name", name);
    query.exec();
    if(query.next())
    {
        return query.value("admin").toBool();
    }
    else {
        
        return false;
    }
    }, name);
}

/*!
 * \fn UserDaoSql::getLastError
 * \deprecated
 */
QString UserDaoSql::getLastError()
{
    return lastError;
}

/*!
 * \fn UserDaoImpl::hashPassword
 * \brief Hachage du mot de passe en paramètre selon le salt.
 * \param password Mot de passe à hacher.
 * \param salt Salt utilisé pour le processus de hachage.
 * \return Le mot de passe haché.
 */
QByteArray UserDaoSql::hashPassword(QString password, QString salt)
{
    return QPasswordDigestor::deriveKeyPbkdf2(QCryptographicHash::Blake2b_512, password.toUtf8(), salt.toUtf8(), 1000, 64);
}
