#ifndef STEPFICHEWIDGET_H
#define STEPFICHEWIDGET_H

#include "qlineedit.h"
#include "qtextedit.h"
#include "stepRecette.h"
#include <QWidget>

namespace Ui {
    class StepFicheWidget;
}

class StepFicheWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StepFicheWidget(stepRecette content,QWidget *parent = nullptr);
    ~StepFicheWidget();

    stepRecette content;

    void Actualise();
    void setFocus(const bool &focus);
    void Push();
    bool Pull(QString *error);

public slots:

    void ChangeType(int);

private:

    Ui::StepFicheWidget *ui;
    QTextEdit *type_recette_texte_texte;
    QLineEdit *type_recette_script_name;
    void mousePressEvent ( QMouseEvent * event );
signals:
    void pressed(StepFicheWidget *source);

};

#endif // STEPFICHEWIDGET_H
