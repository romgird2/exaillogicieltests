#include "TestDaoSql.h"
#include "qsqlerror.h"

/*!
 * \fn TestDaoSql::TestDaoImpl
 * \param db
 */
TestDaoSql::TestDaoSql(AsyncDatabaseAccess& db) : TestDao(db)
{

}

/*!
 * \fn TestDaoSql::getTest
 * \brief Recherche le test dans la base de données correspondant à \a l'id fourni.
 * \param id ID du test recherché.
 * \return Le test s'il y a correspondance, une erreur dans le cas contraire.
 */
QFuture<TestResult> TestDaoSql::getTest(const int &id)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](int id){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Test\" WHERE id=:id");
            query.bindValue(":id", id);
            TestResult result;
            if(query.next()){
                result.test.id = id;
                result.test.date = query.value("date").toDate();
                result.test.data = query.value("data").toString();
                result.test.batterie = query.value("batterie").toString();
                result.test.fiche_recette = query.value("fiche_recette").toInt();
                result.test.user = query.value("trig_utilisateur").toString();
                result.test.sn = query.value("sn").toString();
                result.error = NO_ERROR;
                return result;
            }
            else {
                if(query.lastError().isValid()){
                    result.error = SQL_ERROR;
                }
                else {
                    result.error = INVALID_ID;
                }
                return result;
            }
    }, id);
}

/*!
 * \fn TestDaoSql::addTest
 * \brief Ajoute un test à la base de données.
 * \param test Le test à ajouter à la base de données.
 * \return Rien si le processus d'ajout se déroule correctement, sinon une erreur dans le cas contraire (une erreur liée à la connexion à la base de données).
 */
QFuture<TestDao::Error> TestDaoSql::addTest(const Test &test)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](Test test){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("INSERT INTO main_db.\"Test\" (date,data,batterie,fiche_recette,trig_utilisateur,sn) VALUES (?,?,?,?,?,?)");
            query.addBindValue(test.date);
            query.addBindValue(test.data);
            query.addBindValue(test.batterie);
            query.addBindValue(test.fiche_recette);
            query.addBindValue(test.user);
            query.addBindValue(test.sn);
            query.exec();
            if(query.lastError().isValid()){
                return SQL_ERROR;
            }
            else {
                return NO_ERROR;
            }
        }, test);
}

/*!
 * \fn TestDaoSql::removeTest
 * \brief Supprime le test de la base de données correspondant à \a l'id fourni.
 * \param id ID du test que l'on souhaite supprimer.
 * \return Rien si le processus de suppression se déroule correctement, sinon une erreur dans le cas contraire (une erreur liée à la connexion à la base de données).
 */
QFuture<TestDao::Error> TestDaoSql::removeTest(const int &id)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](int id){
            QSqlQuery query("DELETE FROM main_db.\"Test\" WHERE id=?");
            query.addBindValue(id);
            query.exec();
            if(query.lastError().isValid()){
                return SQL_ERROR;
            }
            else {
                return NO_ERROR;
            }
        }, id);
}

/*!
 * \fn TestDaoSql::checkUser
 * \brief Vérifie qu'un utilisateur de trigramme donné peut suivre ou non une fiche recette d'ID donné.
 * \param trigramme Le trigramme de l'utilisateur dont on souhaite vérifier les compétences.
 * \param ficheId L'ID de la fiche recette dont on souhaite vérifier les privilèges.
 * \return True si l'utilisateur peut suivre cette fiche recette, False dans le cas contraire.
 */
QFuture<bool> TestDaoSql::checkUser(const QString &trigramme, const int &ficheId)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](QString trigramme, int ficheId){
        QSqlQuery query(QSqlDatabase::database("main-db"));
        query.prepare("SELECT * FROM main_db.\"Test\" WHERE trig_utilisateur=? AND fiche_recette=?");
        query.addBindValue(trigramme);
        query.addBindValue(ficheId);
        query.exec();
        return query.size() >= N_TEST_COMPETENCE;
        }, trigramme, ficheId);
}
