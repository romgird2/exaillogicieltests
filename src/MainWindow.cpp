#include "MainWindow.h"
#include "./ui_Ui_MainWindow.h"
#include "FicheRecetteDaoSql.h"
#include "QSqlRecord.h"
#include "UserDaoSql.h"
#include "informationtests.h"
#include "jsoneditor.h"
#include "qmenubar.h"
#include "qsqldatabase.h"
#include "style.h"
#include "connexiondialog.h"
#include "usereditor.h"
#include <QRegularExpression>
#include <QTextEdit>

QRegularExpression MainWindow::regex("^]D1240(\\w+)\\]91.\\]92.{1,2}\\]21(\\w+)$");

/*!
 * \fn MainWindow::MainWindow
 * \brief Constructeur de la fenêtre principale.
 * \param db Base de données utilisée.
 * \param windowParams Les paramètres liés à l'instanciation de la fenêtre principale.
 * \param parent
 */
MainWindow::MainWindow(AsyncDatabaseAccess & db, MainWindowV windowParams, QWidget *parent)
    : QDialog(parent, Qt::Window)
    , ui(new Ui::MainWindow)
    , qrscan_ok(":/images/check.png")
    , qrscan_erreur(":/images/croix.png")
    , db(db)
{

    ui->setupUi(this);
    Style::putStyle(this);
    testDbAccess = std::make_unique<TestDaoSql>(db);
    carteDbAccess = std::make_unique<CarteElecDaoSql>(db);
    ficheDbAccess = std::make_unique<FicheRecetteDaoSql>(db);
    userDbAccess = std::make_unique<UserDaoSql>(db);
    ofDbAccess = std::make_unique<OrdreFabricationDaoSql>(db);
    ui->qrcodeimage->setStyleSheet("QLabel { color : blue; }");

    QFile *file;
    file = new QFile(":/style/bouton_ok.qss");
    file->open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file->readAll());
    ui->pushButtonOk->setStyleSheet(styleSheet);
    file->close();
    ui->pushButtonOk->ensurePolished();

    ui->labelErrorSN->hide();
    ui->pushButtonScan->setShortcut(QKeySequence(Qt::Key_S));
    this->setWindowTitle(QString("Bienvenue %1").arg(windowParams.trigramme));
    connect(ui->pushButtonOk, &QPushButton::clicked, this, &MainWindow::fetchCard);
    connect(this,&MainWindow::hideErrorLabel, this, [=](){ui->labelErrorSN->hide();});
    connect(this,&MainWindow::changeErrorLabel, this, [=](QString text){ui->labelErrorSN->show(); ui->labelErrorSN->setText(text);});
    connect(this,&MainWindow::setPnField, this, [=](QString pn){ui->lineEditPN->setText(pn);});
    connect(this,&MainWindow::changeFicheStatus, this, [=](QString status, QString name, QString date, QString auteur){
        ui->labelFicheStatus->setText(status);
        ui->label_fichenom->setText(name);
        ui->label_fichedate->setText(date);
        ui->label_ficheuser->setText(auteur);
    });
    connect(this,&MainWindow::requestUserAbilities, this, &MainWindow::checkUserAbilities);
    connect(this,&MainWindow::askForTutor, this, &MainWindow::displayTutorRequest);
    connect(this,&MainWindow::openMainProduction, this, [this](int ficheId){
        returnCode = MainProductionV{userTrigramme,isAdmin, ficheId, ui->lineEditPN->text(), ofFound, ui->lineEditSN->text()};
        QDialog::accept();
    });
    userTrigramme = windowParams.trigramme;
    isAdmin = windowParams.isAdmin;

    ChangeState(1);
    ui->label_4->setVisible(false);
    if(windowParams.of_field != ""){
        ui->lineEditOF->setText(windowParams.of_field);
        ui->lineEditOF->setEnabled(false);
    }
    if(windowParams.pn_field != ""){
        ui->lineEditPN->setText(windowParams.pn_field);
        ui->lineEditPN->setEnabled(false);
    }




    if(isAdmin)
    {
        QMenuBar *bar = new QMenuBar(this);
        gestion = new QMenu("Gestion",bar);
        bar->addMenu(gestion);
        gestion->setVisible(isAdmin);

        QAction *ficheAction = gestion->addAction("Fiches recettes");
        QAction *userAction = gestion->addAction("Utilisateurs");
        QAction *statsAction = gestion->addAction("Statistiques");

        connect(ficheAction,SIGNAL(triggered()),this,SLOT(DisplayJsonEditor()));
        connect(userAction,SIGNAL(triggered()),this,SLOT(DisplayUserEditor()));
        connect(statsAction,&QAction::triggered,this,[this](){
            InformationTests* testsInfo = new InformationTests(this->db,this);
            testsInfo->show();
        });
    }
    else
        scanDialog();



}

/*!
 * \fn MainWindow::DisplayJsonEditor
 * \brief Créé une instance de l'éditeur JSON et l'affiche
 */
void MainWindow::DisplayJsonEditor()
{
    jsonEditor *edit = new jsonEditor(db,userTrigramme,this);
    edit->show();
}

/*!
 * \fn MainWindow::DisplayUserEditor
 */
void MainWindow::DisplayUserEditor()
{
    UserEditor *user = new UserEditor(db,userTrigramme,this);
    user->show();
}

/*!
 * \fn MainWindow::ChangeState
 * \brief Change l'état par rapport à la lecture du QR Code (Erreur de lecture, lecture en cours, lecture réussie)
 * \param state L'ID de l'état (0 : erreur, 1 : en cours, 2 : ok)
 */
void MainWindow::ChangeState(int state)
{
    switch(state)
    {
    case 0: // Erreur
        ui->qrcodeimage->setText("");
        ui->qrcodeimage->setPixmap(qrscan_erreur);
        ui->qrcode_message->setText("Erreur");
        ui->qrcode_message->setStyleSheet("QLabel { color : red; }");
        break;
    case 1: // En cours
        ui->qrcodeimage->setText("...");
        ui->qrcode_message->setText("En cours");
        ui->qrcode_message->setStyleSheet("QLabel { color : blue; }");
        break;
    case 2: // Ok
        ui->qrcodeimage->setText("");
        ui->qrcodeimage->setPixmap(qrscan_ok);
        ui->qrcode_message->setText("Ok");
        ui->qrcode_message->setStyleSheet("QLabel { color : green; }");
        break;

    }
}

/*!
 * \fn MainWindow::~MainWindow
 * \brief Destructeur de la fenêtre principal.
 */
MainWindow::~MainWindow()
{
    delete ui;
}

/*!
 * \fn MainWindow::fetchCard
 * \brief Lance la recherche d'une carte électronique dans la base de données.
 * \return (emit) Remplis le champ du PN et recherche la fiche recette associée (chechFiche) si le processus se déroule correctement, sinon émet une erreur dans la fenêtre dans le cas contraire (soit le SN ne correspond à aucune carte dans la base de données, soit une erreur de connexion à la base de données).
 */
void MainWindow::fetchCard()
{
    carteDbAccess->getCarteElec(ui->lineEditSN->text()).then([this](CarteElecResult res){
        if(res.error == CarteElecDao::NO_ERROR){
            emit hideErrorLabel();
            emit setPnField(res.carte_elec.PN);
            checkOf(ui->lineEditOF->text());
        }
        else if(res.error == CarteElecDao::INVALID_SN){
            emit changeErrorLabel("Le SN renseigné est invalide");
        }
        else if(res.error == CarteElecDao::SQL_ERROR){
            emit changeErrorLabel("Une erreur de connexion a eu lieu");
        }
    });
}

/*!
 * \fn MainWindow::checkFiche
 * \brief Vérifie qu'une fiche recette existe dans la base de données correspondant au \a PN fourni.
 * \param pn Le PN de la fiche recette que l'on recherche.
 * \brief (emit) Annonce qu'une fiche recette a été trouvée et vérifie que l'utilisateur peut la suivre (checkUserAbilities), sinon annonce que la fiche recette associée est introuvable.
 */
void MainWindow::checkFiche(const QString &pn)
{
    ficheDbAccess->getLastFicheRecette(pn).then([this](FicheRecetteResult result){
        if(result.error == FicheRecetteDao::NO_ERROR){
            emit changeFicheStatus("Fiche recette trouvée",result.fiche.name,QLocale::system().toString(result.fiche.Date, "dd/MM/yyyy"),QString("Auteur : %1").arg(result.fiche.Trigramme));
            emit requestUserAbilities(result.fiche.id, false);
        }
        else{
            emit changeFicheStatus("Fiche recette introuvable","","","");
        }
    });
}

/*!
 * \fn MainWindow::checkOf
 * \brief Vérifie si l'ordre de fabrication renseigné est bien existant et est en état de démarrer.
 * \param of L'OF dont on veut vérifier l'existence et l'état.
 * \return Rien si le processus de vérification se déroule correctement, sinon une erreur dans le cas contraire (OF introuvable, OF terminée, PN ne correspondant pas à l'OF).
 */
void MainWindow::checkOf(const QString &of)
{
    ofDbAccess->getOF(of).then([this](OFResult result){
        if(result.error == OrdreFabricationDao::NO_ERROR){
            if(result.of.nb_restant == 0){
                emit changeErrorLabel("Cet ordre de fabrication est déja terminé");
            }
            else if(result.of.PN != ui->lineEditPN->text()){
                emit changeErrorLabel("Cet ordre de fabrication ne concerne pas ce PN.");
            }
            else {
                this->ofFound = result.of;
                checkFiche(result.of.PN);
            }
        }
        else {
            emit changeErrorLabel("Cet ordre de fabrication est introuvable.");
        }
    });
}

/*!
 * \fn MainWindow::checkUserAbilities
 * \brief Vérifie qu'un utilisateur peut suivre la fiche recette d'\a ID donné, bypass si l'utilisateur est un tuteur.
 * \param ficheId ID de la fiche recette dont on veut vérifier les privilèges.
 * \param isConcerningTutor True si l'utilisateur est un tuteur, False sinon.
 */
void MainWindow::checkUserAbilities(const int &ficheId, const bool &isConcerningTutor)
{
    if(isAdmin){
        emit openMainProduction(ficheId);
        return;
    }
    testDbAccess->checkUser(userTrigramme, ficheId).then([this, isConcerningTutor, ficheId](bool isCapable){
        if(!isCapable){
            emit askForTutor(ficheId,isConcerningTutor);
        }
        else {
            emit openMainProduction(ficheId);
        }
    });
}

/*!
 * \fn MainWindow::displayTutorRequest
 * \brief Affiche les informations concernant la vérification que l'utilisateur puisse suivre la fiche recette.
 * \param ficheId ID de la fiche recette dont on vérifie les privilèges.
 * \param isConcerningTutor True si l'utilisateur est un tuteur, False sinon.
 */
void MainWindow::displayTutorRequest(const int &ficheId, const bool &isConcerningTutor)
{
    if(isConcerningTutor)
        QMessageBox::information(nullptr,"Requête","Le tuteur indiqué n'a pas effectué assez de tests avec cette version de la fiche recette. Veuillez réessayer.");
    else
        QMessageBox::information(nullptr,"Requête","Vous n'avez pas encore effectué assez de tests avec cette version de la fiche recette. La connexion d'un tuteur va être demandée.");
    WindowManagerReturn result;
    {
        ConnexionDialog tutorDialog(db);
        tutorDialog.exec();
        result = tutorDialog.managerReturn();
    }
    if(std::holds_alternative<MainWindowV>(result)){
        MainWindowV returnValue = std::get<MainWindowV>(result);
        userTrigramme = returnValue.trigramme;
        emit requestUserAbilities(ficheId, true);
       
    }
}

/*!
 * \fn MainWindow::remplirCasesInterface
 * \brief Remplis les champs de l'interface concernant le PN et le SN.
 * \param PN Le PN à renseigner dans le champ associé.
 * \param SN Le SN à renseigner dans le champ associé.
 */
void MainWindow::remplirCasesInterface(const QString& PN, const QString& SN) {

    ui->lineEditPN->setText(PN);
    ui->lineEditSN->setText(SN);

}

/*!
 * \fn MainWindow::lectureQr
 * \brief Vérification de l'entrée textuelle et sa correspondance au format des QR Code contenant les PN et SN.
 * \param codeQr Entrée textuelle récupérée à la lecture du QR Code
 * \return Remplis les cases de l'interface si l'entrée textuelle est conforme, change l'état de lecture du QR Code sur erreur dans le cas contraire.
 */
void MainWindow::lectureQr(const QString& codeQr)
{
    QRegularExpressionMatch match = regex.match(codeQr);

    if (match.hasMatch()) {

        QString PN = match.captured(1);
        QString SN = match.captured(2);
        remplirCasesInterface(PN, SN);
        ChangeState(2);

    } else {
        ChangeState(0);
        ui->label_4->setVisible(true);
    }



}

/*!
 * \fn MainWindow::scanDialog
 * \brief Met en place la fenêtre invitant l'utilisateur à scanner le QR Code.
 */
void MainWindow::scanDialog() {

    bool ok;
    QString qrCodeData = QInputDialog::getText(this, "QrCode input", "Veuillez scanner le QrCode", QLineEdit::Normal,QString(), &ok);

    if (ok) lectureQr(qrCodeData);
}

/*!
 * \fn MainWindow::on_pushButton_pressed
 * \brief Cette méthode est du deadcode mais est nécessaire pour le bon fonctionnement de la détection de pression du bouton.
 * \deprecated
 */
void MainWindow::on_pushButton_pressed(){
    //BUG
}

/*!
 * \fn MainWindow::on_pushButtonScan_clicked
 * \brief Permet l'ouverture de la fenêtre de dialogue après pression du bouton.
 */
void MainWindow::on_pushButtonScan_clicked()
{
    scanDialog();
}

