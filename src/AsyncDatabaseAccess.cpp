#include "AsyncDatabaseAccess.h"

/*!
 * \fn AsyncDatabaseAccess::AsyncDatabaseAccess(QObject *parent)
 * \param parent
 */
AsyncDatabaseAccess::AsyncDatabaseAccess(QObject *parent) : QObject(parent)
{
    threadPool.setMaxThreadCount(1);
    threadPool.setExpiryTimeout(-1);
}

/*!
 * \fn AsyncDatabaseAccess::open
 * \brief Établie la connexion à la base de données distante
 * \param host nom d'hôte choisi pour la base de données
 * \param port port utilisé par la base de données distante
 * \param name nom de la base de données distante
 * \param user nom d'utilisateur utilisé pour accéder à la base données distante
 * \param password mot de passe utilisé pour accéder à la base données distante
 * \return True si la connection s'est bien passée, False dans le cas contraire
 */
QFuture<bool> AsyncDatabaseAccess::open(const QString &host, const int &port, const QString &name, const QString &user, const QString &password)
{
    return QtConcurrent::run(&threadPool,[&](){
        QSqlDatabase database = QSqlDatabase::addDatabase("QPSQL","main-db");
        database.setDatabaseName("db_exail");
        database.setUserName("role_exail");
        database.setPassword("mdp_du_role");
        database.setPort(5432);
        database.setHostName("192.168.1.1");
        database.setConnectOptions("connect_timeout=10");
        return database.open();
    });
}

/*!
 * \fn AsyncDatabaseAccess::getThreadPoolPtr
 * \brief Permet d'accéder au pointeur du thread pool (consistitué d'un seul thread) pour exécuter une requête avec QtConcurrent::Run
 * \return Le pointeur du thread pool référencé par la classe
 */
QThreadPool* AsyncDatabaseAccess::getThreadPoolPtr()
{
    return &threadPool;
}


/*!
 * \fn AsyncDatabaseAccess::~AsyncDatabaseAccess
 * \brief Effectue la fermeture propre de la base de données
 */
AsyncDatabaseAccess::~AsyncDatabaseAccess()
{
    QSqlDatabase::database().close();
    threadPool.waitForDone();
}
