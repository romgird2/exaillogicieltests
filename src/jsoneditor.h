#ifndef JSONEDITOR_H
#define JSONEDITOR_H

#include "FicheRecetteDao.h"
#include "RegisteredWindow.h"
#include "UserDaoSql.h"
#include "AsyncDatabaseAccess.h"
#include "qdialog.h"
#include <QDialog>
#include "qtreewidget.h"

namespace Ui {
class jsonEditor;
}

class jsonEditor : public QDialog, public RegisteredWindow
{
    Q_OBJECT

public:
    explicit jsonEditor(AsyncDatabaseAccess & db, QString userTrigramme,QWidget *parent = nullptr);
    ~jsonEditor();

public slots:
    void openFicheEditor(QTreeWidgetItem* item, const int &x);
    void setFicheList(const QList<FicheRecetteResult> &list);
    void createNew();

private:
    Ui::jsonEditor *ui;
    std::unique_ptr<FicheRecetteDao> ficheDbAccess;
    void showMenu(const QPoint & pos);
    QString userTrigramme;
signals:
    void ficheListSent(QList<FicheRecetteResult> list);
private slots:
    void on_actionOpen_triggered();
    void on_actionDelete_triggered();
};

#endif // JSONEDITOR_H
