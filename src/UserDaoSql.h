#ifndef USERDAOSQL_H
#define USERDAOSQL_H

#include "UserDao.h"
#include <QPasswordDigestor>
#include <QRandomGenerator64>

class UserDaoSql : public UserDao
{
public:
    explicit UserDaoSql(AsyncDatabaseAccess& db);
    virtual QFuture<UserResult> getUser(const QString &name, const QString &password) override;
    virtual QFuture<QList<User>> getListUsers() override;
    virtual QFuture<Error> addUser(const User &user, const QString &password) override;
    virtual QFuture<Error> removeUser(const QString &name) override;
    virtual QFuture<bool> isAdmin(const QString &name) override;
    virtual QString getLastError() override;
    virtual QFuture<Error> alterUser(const User &user);

protected:
    static QByteArray hashPassword(QString password, QString salt);
};

#endif // USERDAOSQL_H
