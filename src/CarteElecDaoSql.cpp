#include "CarteElecDaoSql.h"

/*!
 * \fn CarteElecDaoSql::CarteElecDaoSql
 * \param ada
 */
CarteElecDaoSql::CarteElecDaoSql(AsyncDatabaseAccess& ada)
    : CarteElecDao{ada}
{

}

/*!
 * \fn CarteElecDaoSql::getCarteElec
 * \brief Recherche une carte électronique dans la base de données correspondant au \a SN fourni.
 * \param SN Numéro de série de la carte électronique recherchée.
 * \return Les informations de la carte électronique s'il y a correspondance, une erreur dans le cas contraire.
 */
QFuture<CarteElecResult> CarteElecDaoSql::getCarteElec(const QString &SN)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](QString SN){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Carte_Electronique\" WHERE sn=?");
            query.addBindValue(SN);
            query.exec();
            CarteElecResult res;
            res.error = NO_ERROR;
            if(query.next()){
                res.carte_elec.SN = SN;
                res.carte_elec.PN = query.value("PN").toString();
                return res;
            }
            else {
               
                res.error = INVALID_SN;
                return res;
            }
        }, SN);
}

/*!
 * \fn CarteElecDaoSql::addCarteElec
 * \brief Ajoute une carte électronique à la base de données.
 * \param carte_elec La carte électronique à ajouter à la base de données.
 * \return Rien si le processus d'ajout se déroule correctement, sinon une erreur dans le cas contraire (soit la carte existe déjà, soit une erreur liée à la connexion à la base de données).
 */
QFuture<CarteElecDao::Error> CarteElecDaoSql::addCarteElec(const CarteElec &carte_elec)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](CarteElec carte_elec){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Carte_Electronique\" WHERE sn=?");
            query.addBindValue(carte_elec.SN);
            query.exec();
            if(query.next()){
                return Error::CARTE_ALREADY_EXISTS;
            }
            else {
                QSqlQuery queryCreation;
                queryCreation.prepare("INSERT INTO Carte_Electronique (sn, pn) VALUES (:sn, :pn)");
                query.bindValue(":sn", carte_elec.SN);
                query.bindValue(":pn", carte_elec.PN);
                query.exec();
                if(query.lastError().isValid()){
                    return Error::SQL_ERROR;
                }
                return Error::NO_ERROR;
            }
        }, carte_elec);
}

/*!
 * \fn CarteElecDaoSql::removeCarteElec
 * \brief Supprime la carte électronique de la base de données correspondant au \a SN fourni.
 * \param SN Numéro de série de la carte électronique que l'on souhaite supprimer.
 * \return Rien si le processus de suppression se déroule correctement, sinon une erreur dans le cas contraire (soit la carte n'existe pas dans la base de données, soit une erreur liée à la connexion à la base de données).
 */
QFuture<CarteElecDao::Error> CarteElecDaoSql::removeCarteElec(const QString &SN)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](QString SN){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Carte_Electronique\" WHERE sn=?");
            query.addBindValue(SN);
            query.exec();
            if(query.next()){
                return (Error::INVALID_SN);
            }
            else {
                QSqlQuery queryCreation;
                queryCreation.prepare("DELETE FROM main_db.\"Carte_Electronique\" WHERE sn=?");
                queryCreation.addBindValue(SN);
                queryCreation.exec();
                if(queryCreation.lastError().isValid()){
                    return (Error::SQL_ERROR);
                }
                return Error::NO_ERROR;
            }
        }, SN);
}

/*!
 * \fn CarteElecDaoImpl::getLastError
 * \deprecated
 */
QString CarteElecDaoSql::getLastError()
{
    return lastError;
}
