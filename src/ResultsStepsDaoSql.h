#ifndef RESULTSSTEPSDAOSQL_H
#define RESULTSSTEPSDAOSQL_H

#include "ResultsStepsDao.h"

class ResultsStepsDaoSql : public ResultsStepsDao
{
public:
    explicit ResultsStepsDaoSql(AsyncDatabaseAccess& db);
    virtual QFuture<ResultMResult> getResultM(const int &id_fiche, const int &pos_in_fiche);
    virtual QFuture<ResultSResult> getResultS(const int &id_fiche, const int &pos_in_fiche);
    virtual QFuture<std::shared_ptr<ResultMResult>> getAllResultM(const int &id_fiche);
    virtual QFuture<Error> addResultM(const ResultM &res_m);
    virtual QFuture<Error> addResultS(const ResultS &res_s);
    virtual QFuture<Error> removeResultM(const int &id_fiche, const int &pos_in_fiche);
    virtual QFuture<Error> removeResultS(const int &id_fiche, const int &pos_in_fiche);
    virtual QFuture<Error> modifyResultM(const ResultM& res_m);
    virtual QFuture<Error> modifyResultS(const ResultS& res_s);
    virtual QString getLastError();

};

#endif // RESULTSSTEPSDAOSQL_H
