#ifndef MAINPRODUCTION_H
#define MAINPRODUCTION_H

#include "FicheRecetteDaoSql.h"
#include "OrdreFabricationDaoSql.h"
#include "TestDaoSql.h"
#include "RegisteredWindow.h"
#include "AsyncDatabaseAccess.h"
#include "recette.h"
#include <QDialog>
#include <QProcess>

namespace Ui {
class MainProduction;
}

class MainProduction : public QDialog, public RegisteredWindow
{
    Q_OBJECT

public:
    explicit MainProduction(AsyncDatabaseAccess & db, MainProductionV & params, QWidget *parent = nullptr);
    ~MainProduction();
    void setupRecette(QString json);

private:
    int currentStepIndex = 0;
    int nextIndexIncrease = 1;
    Ui::MainProduction *ui;
    std::unique_ptr<FicheRecetteDao> ficheDbAccess;
    std::unique_ptr<OrdreFabricationDao> ofDbAccess;
    std::unique_ptr<TestDao> testDbAccess;
    std::unique_ptr<Recette> recette;
    QList<stepRecette> succededSteps;
    QList<stepRecette> failedSteps;
    QProcess* currentScript;
    bool scriptResult = false;
    bool isAdmin = false;
    QString trigramme;
    OF currentOf;
    QString pnText;
    QString snText;
    int id_fiche;
    void fillListWidget();
    void setUpCenterWidget();
    void setNextStep();
    void handleScriptOutput();
private:
    signals:
    void ficheRecieved(QString json);
    void returnToMainWindow();
};

#endif // MAINPRODUCTION_H
