#ifndef FICHERECETTEDAO_H
#define FICHERECETTEDAO_H

#include <QObject>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include "AsyncDatabaseAccess.h"

struct FicheRecette {
    int id;
    QString name;
    QString PN;
    QDate Date;
    QString Trigramme;
    QString json;
};
struct FicheRecetteResult;



class FicheRecetteDao
{
public:
    explicit FicheRecetteDao(AsyncDatabaseAccess& db);
    virtual ~FicheRecetteDao();
    enum Error {
        INVALID_PN,
        INVALID_NAME,
        FICHE_ALREADY_EXISTS,
        SQL_ERROR,
        NO_ERROR
    };
    virtual QFuture<FicheRecetteResult> getFicheRecette(const int &id) = 0;
    virtual QFuture<FicheRecetteResult> getLastFicheRecette(const QString &pn) = 0;
    virtual QFuture<Error> addFicheRecette(const FicheRecette &fiche) = 0;
    virtual QFuture<Error> removeFicheRecette(const int &id) = 0;
    virtual QString getLastError() = 0;
    virtual QFuture<QList<FicheRecetteResult>> getListFichesRecettes() = 0;
    virtual QFuture<QString> getJsonRecette(const QString &name) = 0;
    virtual QFuture<QString> getJsonRecette(const int &id) = 0;
    virtual QFuture<Error> alterFicheRecette(const FicheRecette &fiche) = 0;


protected:
    QString lastError;
    AsyncDatabaseAccess& db;

};

struct FicheRecetteResult{
    FicheRecette fiche;
    FicheRecetteDao::Error error;
};






#endif // FICHERECETTEDAO_H
