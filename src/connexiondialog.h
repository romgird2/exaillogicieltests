#ifndef CONNEXIONDIALOG_H
#define CONNEXIONDIALOG_H

#include <QDialog>
#include <QFile>
#include <QPasswordDigestor>
#include "RegisteredWindow.h"
#include "UserDaoSql.h"

namespace Ui {
class ConnexionDialog;
}

class ConnexionDialog : public QDialog, public RegisteredWindow
{
    Q_OBJECT

public:
    explicit ConnexionDialog(AsyncDatabaseAccess &db, QWidget *parent = nullptr);
    ~ConnexionDialog();
    void verifyEntries();
    void verifyUser(const bool &competence);

signals:
    void confirmUser(bool isAdmin);

private:
    Ui::ConnexionDialog *ui;
    std::unique_ptr<UserDao> userDbAccess;
    //QByteArray hashPassword(QString password);
};

#endif // CONNEXIONDIALOG_H
