#ifndef NEWUSER_H
#define NEWUSER_H

#include "AsyncDatabaseAccess.h"
#include "UserDao.h"
#include <QDialog>

namespace Ui {
class NewUser;
}

class NewUser : public QDialog
{
    Q_OBJECT

public:
    explicit NewUser(UserDao &userDbAccess,QWidget *parent = nullptr);
    ~NewUser();

public slots:
    bool Save();
    void CloseAndSave();

signals:
    void UpdatingUserNeeded();

private:
    Ui::NewUser *ui;
    UserDao &userDbAccess;
};

#endif // NEWUSER_H
