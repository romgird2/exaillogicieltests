#include "ResultsStepsDaoSql.h"

/*!
 * \fn ResultsStepsDaoSql::ResultsStepsDaoSql
 * \param ada
 */
ResultsStepsDaoSql::ResultsStepsDaoSql(AsyncDatabaseAccess &ada) : ResultsStepsDao{ada}
{

}

/*!
 * \fn ResultsStepsDaoSql::getResultM
 * \brief Récupère les résultats de l'une des étapes "mesure" d'une fiche recette demandée.
 * \param id_fiche ID de la fiche recette dont on souhaite récupérer l'un des résultats.
 * \param pos_in_fiche Position de l'étape "mesure" dont on souhaite récupérer les résultats.
 * \return Les informations relatives aux résultats demandés.
 */
QFuture<ResultMResult> ResultsStepsDaoSql::getResultM(const int &id_fiche, const int &pos_in_fiche)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](int id_fiche, int pos_in_fiche){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Resultat_Mesures\" WHERE id_fiche=? AND pos_in_fiche=?");
            query.addBindValue(id_fiche);
            query.addBindValue(pos_in_fiche);
            query.exec();
            ResultMResult res;
            res.error = NO_ERROR;
            while(query.next()){
                ResultM data;
                data.id_fiche = id_fiche;
                data.pos_in_fiche = pos_in_fiche;
                data.min_value = query.value("min_value").toDouble();
                data.max_value = query.value("max_value").toDouble();
                data.mesure = query.value("mesure").toDouble();
                res.res_m.append(data);
            }
            if(res.res_m.size() > 0 ) {
                return res;
            }
            else {
                res.error = INVALID_ID_FICHE;
                return res;
            }
        }, id_fiche, pos_in_fiche);
}

/*!
 * \fn ResultsStepsDaoSql::getResultS
 * \brief Récupère les résultats de l'une des étapes "scrippt" d'une fiche recette demandée.
 * \param id_fiche ID de la fiche recette dont on souhaite récupérer l'un des résultats.
 * \param pos_in_fiche Position de l'étape "script" dont on souhaite récupérer les résultats.
 * \return Les informations relatives aux résultats demandés.
 */
QFuture<ResultSResult> ResultsStepsDaoSql::getResultS(const int &id_fiche, const int &pos_in_fiche)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](int id_fiche, int pos_in_fiche){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Resultat_Scripts\" WHERE id_fiche=? AND pos_in_fiche=?");
            query.addBindValue(id_fiche);
            query.addBindValue(pos_in_fiche);
            query.exec();
            ResultSResult res;
            res.error = NO_ERROR;
            if(query.next()){
                res.res_s.id_fiche = id_fiche;
                res.res_s.pos_in_fiche = pos_in_fiche;
                res.res_s.script_status = query.value("script_status").toBool();
                res.res_s.script_log = query.value("script_log").toString();
                return res;
            }
            else {
                
                res.error = INVALID_ID_FICHE;
                return res;
            }
        }, id_fiche, pos_in_fiche);
}

/*!
 * \fn ResultsStepsDaoSql::getAllResultM
 * \brief Récupère les résultats d'une fiche recette spécifiée.
 * \param id_fiche La fiche recette dont on souhaite récupérer les résultats.
 * \return Les résultats d'une fiche recette.
 */
QFuture<std::shared_ptr<ResultMResult>> ResultsStepsDaoSql::getAllResultM(const int &id_fiche)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](int id_fiche){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Resultat_Mesures\" WHERE id_fiche=?");
            query.addBindValue(id_fiche);
            query.exec();
            auto res = std::make_shared<ResultMResult>();
            res->error = NO_ERROR;
            while(query.next()){
                ResultM data;
                data.id_fiche = id_fiche;
                data.pos_in_fiche = query.value("pos_in_fiche").toDouble();
                data.min_value = query.value("min_value").toDouble();
                data.max_value = query.value("max_value").toDouble();
                data.mesure = query.value("mesure").toDouble();
                res->res_m.append(data);
            }
            if(res->res_m.size() > 0 ) {
                return res;
            }
            else {
                res->error = INVALID_ID_FICHE;
                return res;
            }
        }, id_fiche);
}

/*!
 * \fn ResultsStepsDaoSql::addResultM
 * \brief Ajoute un résultat "mesure" à la base de données.
 * \param res_m Le résultat type "mesure" à ajouter à la base de données.
 * \return Rien si le processus d'ajout se déroule correctement, sinon une erreur dans le cas contraire.
 */
QFuture<ResultsStepsDao::Error> ResultsStepsDaoSql::addResultM(const ResultM &res_m)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](ResultM res_m){
            QSqlQuery queryCreation(QSqlDatabase::database("main-db"));
            queryCreation.prepare("INSERT INTO main_db.\"Resultat_Mesures\" (id_fiche, pos_in_fiche, min_value, max_value, mesure) VALUES (:id_fiche, :pos_in_fiche, :min_value, :max_value, :mesure)");
            queryCreation.bindValue(":id_fiche", res_m.id_fiche);
            queryCreation.bindValue(":pos_in_fiche", res_m.pos_in_fiche);
            queryCreation.bindValue(":min_value", res_m.min_value);
            queryCreation.bindValue(":max_value", res_m.max_value);
            queryCreation.bindValue(":mesure", res_m.mesure);
            queryCreation.exec();
            if(queryCreation.lastError().isValid()){
                return Error::SQL_ERROR;
            }
            return Error::NO_ERROR;
    }, res_m);
}

/*!
 * \fn ResultsStepsDaoSql::addResultS
 * \brief Ajoute un résultat "script" à la base de données.
 * \param res_s Le résultat type "script" à ajouter à la base de données.
 * \return Rien si le processus d'ajout se déroule correctement, sinon une erreur dans le cas contraire.
 */
QFuture<ResultsStepsDao::Error> ResultsStepsDaoSql::addResultS(const ResultS &res_s)
{
    return QtConcurrent::run(db.getThreadPoolPtr(), [](ResultS res_s){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Resultat_Scripts\" WHERE id_fiche=? AND pos_in_fiche=?");
            query.addBindValue(res_s.id_fiche);
            query.addBindValue(res_s.pos_in_fiche);
            query.exec();
            if(query.next()){
                return Error::RESULT_MESURE_ALREADY_EXISTS;
            }
            else {
                QSqlQuery queryCreation;
                queryCreation.prepare("INSERT INTO Resultat_Scripts (id_fiche, pos_in_fiche, script_status, script_log) VALUES (:id_fiche, :pos_in_fiche, :script_status, :script_log)");
                queryCreation.bindValue(":id_fiche", res_s.id_fiche);
                queryCreation.bindValue(":pos_in_fiche", res_s.pos_in_fiche);
                queryCreation.bindValue(":script_status", res_s.script_status);
                queryCreation.bindValue(":script_log", res_s.script_log);
                queryCreation.exec();
                if(queryCreation.lastError().isValid()){
                    return Error::SQL_ERROR;
                }
                return Error::NO_ERROR;
            }
        }, res_s);
}

/*!
 * \fn ResultsStepsDaoSql::removeResultM
 * \brief Supprime un résultat type "mesure" d'une fiche recette donné.
 * \param id_fiche L'ID de la fiche recette dont on souhaite supprimer l'un des résultats.
 * \param pos_in_fiche La position de l'étape dont le résultat doit être supprimé.
 * \return Rien si le processus de suppression se déroule correctement, sinon une erreur dans le cas contraire.
 */
QFuture<ResultsStepsDao::Error> ResultsStepsDaoSql::removeResultM(const int &id_fiche, const int &pos_in_fiche)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](int id_fiche, int pos_in_fiche){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Resultat_Mesures\" WHERE id_fiche=? AND pos_in_fiche=?");
            query.addBindValue(id_fiche);
            query.addBindValue(pos_in_fiche);
            query.exec();
            if(query.next()){
                QSqlQuery queryCreation;
                queryCreation.prepare("DELETE FROM main_db.\"Resultat_Mesures\" WHERE id_fiche=? AND pos_in_fiche=?");
                queryCreation.addBindValue(id_fiche);
                queryCreation.addBindValue(pos_in_fiche);
                queryCreation.exec();
                if(queryCreation.lastError().isValid()){
                    //lastError = query.lastError().text();
                    return (Error::SQL_ERROR);
                }
                return Error::NO_ERROR;

            }
            else {
                return (Error::INVALID_ID_FICHE);
            }
        }, id_fiche, pos_in_fiche);
}

/*!
 * \fn ResultsStepsDaoSql::removeResultS
 * \brief Supprime un résultat type "script" d'une fiche recette donné.
 * \param id_fiche L'ID de la fiche recette dont on souhaite supprimer l'un des résultats.
 * \param pos_in_fiche La position de l'étape dont le résultat doit être supprimé.
 * \return Rien si le processus de suppression se déroule correctement, sinon une erreur dans le cas contraire.
 */
QFuture<ResultsStepsDao::Error> ResultsStepsDaoSql::removeResultS(const int &id_fiche, const int &pos_in_fiche)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](int id_fiche, int pos_in_fiche){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Resultat_Scripts\" WHERE id_fiche=? AND pos_in_fiche=?");
            query.addBindValue(id_fiche);
            query.addBindValue(pos_in_fiche);
            query.exec();
            if(query.next()){
                QSqlQuery queryCreation;
                queryCreation.prepare("DELETE FROM main_db.\"Resultat_Scripts\" WHERE id_fiche=? AND pos_in_fiche=?");
                queryCreation.addBindValue(id_fiche);
                queryCreation.addBindValue(pos_in_fiche);
                queryCreation.exec();
                if(queryCreation.lastError().isValid()){
                    return (Error::SQL_ERROR);
                }
                return Error::NO_ERROR;

            }
            else {
                return (Error::INVALID_ID_FICHE);
            }
        }, id_fiche, pos_in_fiche);
}

/*!
 * \fn ResultsStepsDaoSql::modifyResultM
 * \brief Modifie un résultat type "mesure".
 * \param res_m Le résultat type "mesure" que l'on souhaite modifier.
 * \return Rien si le processus de modification se déroule correctement, sinon une erreur dans le cas contraire.
 */
QFuture<ResultsStepsDao::Error> ResultsStepsDaoSql::modifyResultM(const ResultM& res_m)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](ResultM res_m){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Resultat_Mesures\" WHERE id_fiche=? AND pos_in_fiche=?");
            query.addBindValue(res_m.id_fiche);
            query.addBindValue(res_m.pos_in_fiche);
            query.exec();
            if(!query.next()){
                return (Error::INVALID_ID_FICHE);
            }
            else {
                QSqlQuery queryModify(QSqlDatabase::database("main-db"));
                queryModify.prepare("UPDATE main_db.\"Resultat_Mesures\" SET min_value=?, max_value=?, mesure=? WHERE id_fiche=? AND pos_in_fiche=?");
                queryModify.addBindValue(res_m.min_value);
                queryModify.addBindValue(res_m.max_value);
                queryModify.addBindValue(res_m.mesure);
                queryModify.addBindValue(res_m.id_fiche);
                queryModify.addBindValue(res_m.pos_in_fiche);
                queryModify.exec();
                if(queryModify.lastError().isValid()){
                    return (Error::SQL_ERROR);
                }
                return Error::NO_ERROR;
            }
        }, res_m);
}

/*!
 * \fn ResultsStepsDaoSql::modifyResultS
 * \brief Modifie un résultat type "script".
 * \param res_m Le résultat type "script" que l'on souhaite modifier.
 * \return Rien si le processus de modification se déroule correctement, sinon une erreur dans le cas contraire.
 */
QFuture<ResultsStepsDao::Error> ResultsStepsDaoSql::modifyResultS(const ResultS& res_s)
{
    return QtConcurrent::run(db.getThreadPoolPtr(),[](ResultS res_s){
            QSqlQuery query(QSqlDatabase::database("main-db"));
            query.prepare("SELECT * FROM main_db.\"Resultat_Scripts\" WHERE id_fiche=? AND pos_in_fiche=?");
            query.addBindValue(res_s.id_fiche);
            query.addBindValue(res_s.pos_in_fiche);
            query.exec();
            if(!query.next()){
                return (Error::INVALID_ID_FICHE);
            }
            else {
                QSqlQuery queryModify(QSqlDatabase::database("main-db"));
                queryModify.prepare("UPDATE main_db.\"Resultat_Scripts\" SET script_status=?, script_log=? WHERE id_fiche=? AND pos_in_fiche=?");
                queryModify.addBindValue(res_s.script_status);
                queryModify.addBindValue(res_s.script_log);
                queryModify.addBindValue(res_s.id_fiche);
                queryModify.addBindValue(res_s.pos_in_fiche);
                queryModify.exec();
                if(queryModify.lastError().isValid()){
                    return (Error::SQL_ERROR);
                }
                return Error::NO_ERROR;
            }
        }, res_s);
}

/*!
 * \brief OrdreFabricationDaoImpl::getLastError
 * \deprecated
 */
QString ResultsStepsDaoSql::getLastError()
{
    return lastError;
}




