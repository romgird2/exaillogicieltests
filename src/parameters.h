#ifndef PARAMETERS_H
#define PARAMETERS_H

#include "RegisteredWindow.h"
#include "AsyncDatabaseAccess.h"
#include <QDialog>

namespace Ui {
class Parameters;
}

class Parameters : public QDialog, public RegisteredWindow
{
    Q_OBJECT

public:
    explicit Parameters(AsyncDatabaseAccess & db, QWidget *parent = nullptr);
    ~Parameters();

public slots:
    void retour();

private:
    Ui::Parameters *ui;
};

#endif // PARAMETERS_H
