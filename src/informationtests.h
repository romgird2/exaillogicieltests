#ifndef INFORMATIONTESTS_H
#define INFORMATIONTESTS_H

#include <QDialog>
#include <QListWidgetItem>
#include "AsyncDatabaseAccess.h"
#include "RegisteredWindow.h"
#include "TestDaoSql.h"
#include "FicheRecetteDaoSql.h"
#include "style.h"
#include "recette.h"
#include "ResultsStepsDaoSql.h"

namespace Ui {
class InformationTests;
}

class InformationTests : public QDialog, public RegisteredWindow
{
    Q_OBJECT

public:
    explicit InformationTests(AsyncDatabaseAccess & db, QWidget *parent = nullptr);
    ~InformationTests();

private:
    Ui::InformationTests *ui;
    std::unique_ptr<TestDao> testDbAccess;
    std::unique_ptr<FicheRecetteDao> ficheDbAccess;
    std::unique_ptr<ResultsStepsDao> resultsDbAccess;
    std::unique_ptr<Recette> recette;
    std::shared_ptr<ResultMResult> allStepsForCurrentFiche;
    int currentFicheId;
    QString currentStepUnitName;
    void setUpListWidgetFiche(QList<FicheRecette> fiches);
    void setUpListWidgetSteps(QList<QString> steps);
    void fetchSteps(QListWidgetItem* item);
    void setupSteps(QString json);
    void showStepMessageBox(QListWidgetItem* item);
    void requestGraphData(int id_fiche, int pos_in_fiche);
    void setUpGraph(QList<double> values, double min_value, double max_value);
    void setUpFicheGraph(std::shared_ptr<ResultMResult> steps);
    void savePdf();

private:
    signals:
    void listFicheWidgetObtained(QList<FicheRecette> fiches);
    void listStepsObtained(QList<QString> steps);
    void jsonObtained(QString json);
    void valuesObtained(QList<double> values, double min_value, double max_value);
    void allStepsObtained(std::shared_ptr<ResultMResult> steps);
};

#endif // INFORMATIONTESTS_H
