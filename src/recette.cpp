#include "recette.h"

/*!
 * \fn Recette::Recette
 * \brief Constructeur d'une recette à partir d'un JSON donné.
 * \param json Le JSON que l'on souhaite décomposer.
 */
Recette::Recette(QString json) : steps()
{
    QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
    QJsonObject obj = document.object();

    titre = obj["titre"].toString();
    PN = obj["pn"].toString();
    QJsonArray etapes = obj["etapes"].toArray();
    for(QJsonValueRef etape : etapes)
    {
        QJsonObject etape_obj = etape.toObject();
        QString type = etape_obj["type"].toString();
        stepRecette step;
        step.type = Recette::match_type(type);
        switch(step.type)
        {
        case stepRecette::texte:
            step.description = etape_obj["desc"].toString();
            break;
        case stepRecette::script:
            step.name = etape_obj["name"].toString();
            break;
        case stepRecette::mesure:
            QJsonArray points = etape_obj["points"].toArray();
            for(QJsonValueRef point : points)
            {
                step.references.push_back(point.toString());
            }

            QJsonArray valeurs = etape_obj["valeurs"].toArray();
            for(QJsonValueRef valeur : valeurs)
            {
                step.valeurs.push_back(valeur.toDouble());
            }

            step.unit_name = etape_obj["unit"].toString();
            step.unit = Recette::match_unit(step.unit_name);

        }
        steps.append(step);
    }
}

const QString Recette::unit_factor_name[] = {"n","u","m","c","d","k","M","G","P"};
const double Recette::unit_factor[] = {1e-9,1e-6,1e-3,1e-2,1e-1,1e3,1e6,1e9,1e12};
const QString Recette::unit_name[] = {"V","A","Pa","g","m","none"};
const int Recette::nb_unit_factor = 8;
const int Recette::nb_unit = 6;

/*!
 * \fn Recette::toJson
 * \brief Permet de convertir la recette en JSON.
 * \return
 */
QString Recette::toJson()
{
    QJsonObject obj;
    obj["titre"] = titre;
    obj["pn"] = PN;

    QJsonArray etapes;

    for(stepRecette step : steps)
    {
        QJsonObject etape_obj;

        switch(step.type)
        {
        case stepRecette::texte:
            etape_obj["type"] = "description";
            etape_obj["desc"] = step.description;
            break;
        case stepRecette::script:
            etape_obj["type"] = "script";
            etape_obj["name"] = step.name;
            break;
        case stepRecette::mesure:
            etape_obj["type"] = "mesure";

            QJsonArray points;
            for(QString reference : step.references)
            {
                points.append(reference);
            }
            etape_obj["points"] = points;

            QJsonArray valeurs;
            for(double valeur : step.valeurs)
            {
                valeurs.append(valeur);
            }
            etape_obj["valeurs"] = valeurs;
            etape_obj["unit"] = step.unit_name;
            break;
        }
        etapes.append(etape_obj);
    }
    obj["etapes"] = etapes;
    QJsonDocument doc(obj);
    return doc.toJson(QJsonDocument::Compact);
}

/*!
 * \fn Recette::AddStep
 * \brief Permet l'ajout d'une étape à la fiche recette.
 * \param step L'étape à ajouter.
 */
void Recette::AddStep(const stepRecette &step)
{
    steps.push_back(step);
}

/*!
 * \fn Recette::RemoveStep
 * \brief Permet la suppression d'une étape d'index/de position fourni(e) de la fiche recette.
 * \param position L'index/la position de l'étape que l'on souhaite voir supprimer.
 */
void Recette::RemoveStep(const int &position)
{
    steps.erase(steps.begin()+position);
}

/*!
 * \fn Recette::GetStep
 * \brief Permet d'obtenir une étape de la recette correspondant à l'index/la position fourni(e).
 * \param position L'index/la position de l'étape que l'on souhaite obtenir.
 * \return L'étape que l'on souhaite obtenir.
 */
stepRecette Recette::GetStep(const int &position)
{
    return steps[position];
}

/*!
 * \fn Recette::match_type
 * \brief Vérifie le type d'étapes renseigné.
 * \param type Le type de l'étape (format "description", "mesure", "script") à vérifier.
 * \return Le type de l'étape si la comparaison est correcte pour l'un des 3 cas, sinon une exception dans le cas contraire.
 */
stepRecette::type_recette Recette::match_type(const QString &type)
{
    if(type == "description")
        return stepRecette::type_recette::texte;
    if(type == "mesure")
        return stepRecette::type_recette::mesure;
    if(type == "script")
        return stepRecette::type_recette::script;
    throw new QException();
}

/*!
 * \fn Recette::match_unit
 * \brief Vérifie l'unité renseigné.
 * \param type L'unité à vérifier.
 * \return L'unité si la comparaison est correcte pour l'un des cas, sinon une exception dans le cas contraire.
 */
double Recette::match_unit(const QString &unit)
{

    for(int i = 0;i != Recette::nb_unit;++i)
    {
        if(unit.right(unit_name[i].size()) == Recette::unit_name[i])
        {
            QString factor = unit.left(unit.size()-Recette::unit_name[i].size());
            for(int j = 0;j != nb_unit_factor;++j)
            {
                if(factor == Recette::unit_factor_name[j])
                {
                    return unit_factor[j];
                }
            }
            return 1;
        }
    }

}
