#include "parameters.h"
#include "ui_parameters.h"
#include "style.h"

/*!
 * \fn Parameters::Parameters
 * \brief Constructeur de l'interface des paramètres.
 * \param db La base de données.
 * \param parent
 */
Parameters::Parameters(AsyncDatabaseAccess & db, QWidget *parent) :
    QDialog(parent, Qt::Window),
    ui(new Ui::Parameters)
{
    ui->setupUi(this);
    Style::putStyle(this);

    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(retour()));
}

/*!
 * \fn Parameters::~Parameters
 * \brief Destructeur de l'interface des paramètres.
 */
Parameters::~Parameters()
{
    delete ui;
}

/*!
 * \fn Parameters::retour
 * \brief Permet de revenir en arrière.
 */
void Parameters::retour()
{

}
