#ifndef FICHEEDITOR_H
#define FICHEEDITOR_H

#include "FicheRecetteDaoSql.h"
#include "qdialog.h"
#include "stepfichewidget.h"
#include <QWidget>

namespace Ui {
class FicheEditor;
}


class FicheEditor : public QDialog
{
    Q_OBJECT

public:
    explicit FicheEditor(QString name,QString userTrigramme, FicheRecetteDao &ficheDbAccess, QString PN, int id, QWidget *parent = nullptr);
    ~FicheEditor();

    void setProperties(const QString &name,const QString &PN);

public slots:
    void CreateNewStep();
    void DeleteCurrentStep();
    void CopyCurrentStep();

    void Save();
    void SaveAs();
    void setJson(const QString &json);
    void ChangeFocus(StepFicheWidget *src);

private:
    void AddNewStep(const stepRecette &step);
    void notifySave();
    FicheRecetteDao &ficheDbAccess;
    QString ToJson();
    QList<StepFicheWidget*> steps;
    void AddStep(const StepFicheWidget &fichewidget);
    Ui::FicheEditor *ui;
    StepFicheWidget *currentFocus;

    QString PN;
    QString userTrigramme;
    int id;

signals:
    void jsonReceived(QString json);
    void ficheSaved();
};

#endif // FICHEEDITOR_H
