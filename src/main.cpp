#include "connexiondialog.h"
#include "MainWindow.h"
#include "jsoneditor.h"
#include "mainproduction.h"
#include "parameters.h"
#include <QApplication>
#include <QFontDatabase>
#include "AsyncDatabaseAccess.h"
#include "WindowManager.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/images/icon_hq.png"));
    int id = QFontDatabase::addApplicationFont(":/style/Gilroy-Light.otf");
    QFontDatabase::applicationFontFamilies(id);
    QFontDatabase::addApplicationFont(":/style/Gilroy-ExtraBold.ttf");
    int dialogStatus {0};
    AsyncDatabaseAccess adb(nullptr);
    auto fopen = adb.open("10.29.228.215",5432, "db_exail", "role_exail", "");
    fopen.waitForFinished();
    bool opened = fopen.result();
    if(!opened){
        return -1;
    }
    WindowManager windowManager{adb};
    auto returnValue = WindowManagerReturn(ConnexionDialogV{});
    while(!std::holds_alternative<ExitV>(returnValue)){
        returnValue = std::visit(windowManager, returnValue);
    }
    return 0;
}
