#ifndef TESTDAO_H
#define TESTDAO_H

#include "AsyncDatabaseAccess.h"

struct Test {
    int id;
    QDate date;
    QString data;
    QString batterie;
    int fiche_recette;
    QString user;
    QString sn;
};

struct TestResult;

class TestDao
{
public:
    TestDao(AsyncDatabaseAccess& db);
    virtual ~TestDao();
    enum Error {
        NO_ERROR,
        INVALID_ID,
        SQL_ERROR
    };

    virtual QFuture<TestResult> getTest(const int &id) = 0;
    virtual QFuture<Error> addTest(const Test &test) = 0;
    virtual QFuture<Error> removeTest(const int &id) = 0;
    virtual QFuture<bool> checkUser(const QString &trigramme, const int &ficheId) = 0;

protected:
    AsyncDatabaseAccess &db;
};

struct TestResult {
    Test test;
    TestDao::Error error;
};

#endif // TESTDAO_H
