#ifndef STYLE_H
#define STYLE_H

#include "ResultsStepsDao.h"
#include <QWidget>
#include <QFile>

namespace Style
{
    static bool nuit = true;
    void putStyle(QWidget *target);

    std::string GetMyDocumentsFolderPath();

    void GeneratePDF(std::shared_ptr<ResultMResult> data,QWidget *parent);
};
#endif // STYLE_H
