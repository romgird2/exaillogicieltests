#include "connexiondialog.h"
#include "ui_connexiondialog.h"
#include "style.h"

/*!
 * \fn ConnexionDialog::ConnexionDialog
 * \brief Constructeur pour l'interface utilisateur de connexion
 * \param db La base de données
 * \param parent
 */
ConnexionDialog::ConnexionDialog(AsyncDatabaseAccess & db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnexionDialog)
{
    ui->setupUi(this);
    Style::putStyle(this);
    userDbAccess = std::make_unique<UserDaoSql>(db);
    connect(ui->loginButton, &QPushButton::clicked, this, &ConnexionDialog::verifyEntries);
    connect(this, &ConnexionDialog::confirmUser, this, [this](bool isAdmin){
        MainWindowV res;
        res.trigramme = ui->lineEditTrigramme->text();
        res.isAdmin = isAdmin;
        res.dialogStatus = QDialog::Accepted;
        this->returnCode = res;
        QDialog::accept();
    });
}

/*!
 * \fn ConnexionDialog::~ConnexionDialog
 * \brief Effectue la fermeture propre de l'interface utilisateur de connexion
 */
ConnexionDialog::~ConnexionDialog()
{
    delete ui;
}

/*!
 * \fn ConnexionDialog::verifyEntries
 * \brief Valide la connexion de l'utilisateur ou affiche l'erreur détectée préalablement par rapport aux entrées de l'utilisateur
 */
void ConnexionDialog::verifyEntries()
{
    userDbAccess->getUser(ui->lineEditTrigramme->text(), ui->lineEditPassword->text()).then([this](UserResult result){
        if(result.error == UserDao::INVALID_PASSWORD){
            ui->labelConnexionError->setText("Erreur : Le mot de passe est invalide");
        }
        else if(result.error == UserDao::INVALID_NAME) {
            ui->labelConnexionError->setText("Erreur : Le trigramme est invalide");
        }
        else if(result.error == UserDao::SQL_ERROR) {
            ui->labelConnexionError->setText("Erreur : La connexion a renvoyé une erreur");
        }
        else {
            emit confirmUser(result.user.isAdmin);
        }
    });
}
