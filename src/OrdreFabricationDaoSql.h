#ifndef ORDREFABRICATIONDAOSQL_H
#define ORDREFABRICATIONDAOSQL_H

#include "OrdreFabricationDao.h"

class OrdreFabricationDaoSql : public OrdreFabricationDao
{
public:
    explicit OrdreFabricationDaoSql(AsyncDatabaseAccess& db);
    virtual QFuture<OFResult> getOF(const QString &id_text);
    virtual QFuture<Error> addOF(const OF &of);
    virtual QFuture<Error> removeOF(const QString &id_text);
    virtual QFuture<Error> modifyOF(const OF& of);
    virtual QString getLastError();
};


#endif // ORDREFABRICATIONDAOSQL_H
