#ifndef USEREDITORWIDGET_H
#define USEREDITORWIDGET_H

#include "UserDao.h"
#include <QWidget>

namespace Ui {
class UserEditorWidget;
}

class UserEditorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit UserEditorWidget(User user,QWidget *parent = nullptr);
    ~UserEditorWidget();

    User user;

public slots:
    void OpenParameters();


signals:
    void OpeningParameters(User user);

private:
    Ui::UserEditorWidget *ui;
};

#endif // USEREDITORWIDGET_H
