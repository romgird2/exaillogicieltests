var class_user_dao_sql =
[
    [ "addUser", "class_user_dao_sql.html#a55b348d8f93711477d3fc61a4c2701ca", null ],
    [ "getLastError", "class_user_dao_sql.html#a00e48302a8769932f43bbfe483cb4f71", null ],
    [ "getListUsers", "class_user_dao_sql.html#a127fb597aa6b3397d55c960454a2afd0", null ],
    [ "getUser", "class_user_dao_sql.html#a77a96dfee289ac3fab639745b05d69c1", null ],
    [ "isAdmin", "class_user_dao_sql.html#ae9111be03eed756f49318c04c8b2fc59", null ],
    [ "removeUser", "class_user_dao_sql.html#a0a1b9f8d8f090de151c99fc0cb6c9ae2", null ]
];