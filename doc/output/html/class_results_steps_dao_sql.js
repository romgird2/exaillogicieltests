var class_results_steps_dao_sql =
[
    [ "ResultsStepsDaoSql", "class_results_steps_dao_sql.html#a5291fc99596a41881bb06464fa22d975", null ],
    [ "addResultM", "class_results_steps_dao_sql.html#a3ca88573224de6a6a08c649e9b7e9ee7", null ],
    [ "addResultS", "class_results_steps_dao_sql.html#a64fcb261a304c2a2298f00221c188077", null ],
    [ "getAllResultM", "class_results_steps_dao_sql.html#a509d085eedf63ac0d9abea647a6a7137", null ],
    [ "getLastError", "class_results_steps_dao_sql.html#a41cbd7de44f8fd53ab5ac8888e492c05", null ],
    [ "getResultM", "class_results_steps_dao_sql.html#a8c33cee2949eb27cea5d7fb63e3bbc9f", null ],
    [ "getResultS", "class_results_steps_dao_sql.html#a6ad55654d4c80971d104c74fea097f8f", null ],
    [ "modifyResultM", "class_results_steps_dao_sql.html#a1d35c90b54c2c10d3647d7d6be0550f1", null ],
    [ "modifyResultS", "class_results_steps_dao_sql.html#aa9d1d565f4dfe7a9a465594d54e0832f", null ],
    [ "removeResultM", "class_results_steps_dao_sql.html#aa57b2e2c2aa483ea97f6ee8ec306972d", null ],
    [ "removeResultS", "class_results_steps_dao_sql.html#a534afc25e1666929727aed290be14ad4", null ]
];