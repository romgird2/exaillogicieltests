#include "Basic_Tests.h"

void BasicTest::startBasicTest() {
    QTest::qExec(this);
}

void BasicTest::example() {

    QString str = "hello";
    QVERIFY(str.toUpper() == "HELLO");
    QCOMPARE(str.toUpper(), QString("HELLO3"));
}

