#ifndef BASIC_TESTS_H
#define BASIC_TESTS_H

#include <QObject>
#include <QTest>

class BasicTest: public QObject
{
    Q_OBJECT

public slots :
    void startBasicTest();

private slots :
    void example();
};

#include "Basic_Tests.moc"

#endif // BASIC_TESTS_H
