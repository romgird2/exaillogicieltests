#ifndef CONNEXIONDIALOGTESTS_H
#define CONNEXIONDIALOGTESTS_H

#include <QWidget>
#include <QtTest>
#include <QApplication>

class ConnexionDialogTest:public QObject
{
    Q_OBJECT
public:
    void set_mainwindow(QWidget * qw);
public slots:
    void startTest();
private:
    QWidget * m_qw ;

private slots:
    void trigrammeLength();
    void invalidTrigramme();
    void invalidMdp();
    void validCombination();
};

#include "ConnexionDialogTests.moc"

#endif // CONNEXIONDIALOGTESTS_H
