#include "ConnexionDialogTests.h"
#include "Sleep.h"

#include <QLineEdit>
#include <QPushButton>
#include <QLabel>

// Launch every method in private slots
void ConnexionDialogTest::startTest()
{
    QTest::qExec(this);
}

void ConnexionDialogTest::set_mainwindow(QWidget * qw)
{
    m_qw = qw;
}

// Verify that the user can't input more than three letters in the login

void ConnexionDialogTest::trigrammeLength()
{
    QLineEdit * tri = m_qw->findChild<QLineEdit*>("lineEditTrigramme");
    tri->insert("GCDR");
    Sleep::sleep(1);

    QVERIFY(tri->text() == "GCD");
    tri->clear();
}

// Verify that an invalid login and password don't work

void ConnexionDialogTest::invalidTrigramme()
{
    QLineEdit * tri = m_qw->findChild<QLineEdit*>("lineEditTrigramme");
    QLineEdit * mdp = m_qw->findChild<QLineEdit*>("lineEditPassword");
    QLabel * error = m_qw->findChild<QLabel*>("labelConnexionError");
    QPushButton * btn = m_qw->findChild<QPushButton*>("loginButton");

    tri->insert("FFF");
    mdp->insert("mdpRGD");
    QTest::mouseClick(btn, Qt::LeftButton);
    Sleep::sleep(1);

    QVERIFY(error->text() == "Erreur : Le trigramme est invalide");
    tri->clear();
    mdp->clear();
    error->clear();

}

void ConnexionDialogTest::invalidMdp()
{
    QLineEdit * tri = m_qw->findChild<QLineEdit*>("lineEditTrigramme");
    QLineEdit * mdp = m_qw->findChild<QLineEdit*>("lineEditPassword");
    QLabel * error = m_qw->findChild<QLabel*>("labelConnexionError");
    QPushButton * btn = m_qw->findChild<QPushButton*>("loginButton");

    tri->insert("RGD");
    mdp->insert("mdpRGDfalse");
    QTest::mouseClick(btn, Qt::LeftButton);
    Sleep::sleep(1);

    QVERIFY(error->text() == "Erreur : Le mot de passe est invalide");
    tri->clear();
    mdp->clear();
    error->clear();
}

void ConnexionDialogTest::validCombination()
{
    QLineEdit * tri = m_qw->findChild<QLineEdit*>("lineEditTrigramme");
    QLineEdit * mdp = m_qw->findChild<QLineEdit*>("lineEditPassword");
    QLabel * error = m_qw->findChild<QLabel*>("labelConnexionError");
    QPushButton * btn = m_qw->findChild<QPushButton*>("loginButton");

    tri->insert("RGD");
    mdp->insert("mdpRGD");
    QTest::mouseClick(btn, Qt::LeftButton);
    Sleep::sleep(1);

    QVERIFY(error->text() == "");
    tri->clear();
    mdp->clear();
    error->clear();
}

