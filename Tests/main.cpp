#include "ConnexionDialogTests.h"

#include "../src/connexiondialog.h"
#include "../src/AsyncDatabaseAccess.h"

// Test unit

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    AsyncDatabaseAccess adb(nullptr);
    auto fopen = adb.open("10.29.228.215",5432, "db_exail", "role_exail", "");
    fopen.waitForFinished();

    ConnexionDialog window(adb);
    window.show();
    app.processEvents();
    ConnexionDialogTest test;
    test.set_mainwindow(&window);

    QTimer::singleShot(1000, &test, SLOT(startTest()));
    return app.exec();
}
