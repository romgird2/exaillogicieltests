import time
import sys

# fileName = "default.res"

# if(len(sys.argv) > 1):
#     fileName = sys.argv[1]

# with open(fileName,"w+") as f:
#     f.write("OK\n")
#     f.write("[15:48:24] - All tests on gappela-g OK")

print("[PROGRESS]0",file=sys.stdout,flush=True)
time.sleep(1)
print("[PROGRESS]50",file=sys.stdout,flush=True)
time.sleep(1)
print("[PROGRESS]100",file=sys.stdout,flush=True)
# Avoid very rapid writes as Qt Signal/Slots are not extremely fast
# Always write one line per print (no \n or \r).
time.sleep(1)
print("[RESULT]OK,All tests of phase 1 on cappela-g OK",file=sys.stdout,flush=True) # [RESULT]OK or [RESULT]NOK
# If no description after OK, the IHM will skip automatically to the next step, otherwise it shows it and waits for OK Button to be pressed.
